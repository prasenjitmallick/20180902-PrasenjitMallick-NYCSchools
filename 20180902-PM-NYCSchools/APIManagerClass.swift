//
//  APIManagerClass.swift
//
//  Created by Prasenjit on 09/02/18.
//  Copyright © 2018 . All rights reserved.
//

//

import UIKit
import SwiftyJSON

let SchoolDirectoryApiUrl = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
let SATResultsApiUrl = "https://data.cityofnewyork.us/resource/734v-jeq5.json"

class APIManagerClass: NSObject {
    
    
    
    class func getSchoolDirectoryData()
    {
        var schoolDirectoryApiResponse : SchoolDirectoryApiResponse?
        let dictionaryForSchoolDirectory = APIManagerClass.getSchoolDirectoryApiCall()
        var jsonBody = Data()
        do {
            
            //Convert to Data
            jsonBody = try JSONSerialization.data(withJSONObject: dictionaryForSchoolDirectory, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonBody, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                var responseDict: [[String: Any]] = [[:]]
                
                if let data = JSONString.data(using: .utf8) {
                    do {
                        responseDict = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    } catch {
                        print(error.localizedDescription)
                    }
                }
                let objSharedInstance = SharedClass.sharedInstance
                for i in 0...(responseDict.count-1)
                {
                    let jsonResponse = JSON(responseDict[i] as [String: Any])
                    schoolDirectoryApiResponse = SchoolDirectoryApiResponse(json: jsonResponse)
                    objSharedInstance.sharedInstanceForSchoolDirectory.append(schoolDirectoryApiResponse!)
                }
                
                
            }
            
        } catch {
        }
        
    }
    
    class func getSATResultsData()
    {
        var satResultsApiResponse : SATResultsApiResponse?
        let dictionaryForSATResults = APIManagerClass.getSATResultsApiCall()
        var jsonBody = Data()
        do {
            
            //Convert to Data
            jsonBody = try JSONSerialization.data(withJSONObject: dictionaryForSATResults, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonBody, encoding: String.Encoding.utf8) {
                print(JSONString)
                
                var responseDict: [[String: Any]] = [[:]]
                
                if let data = JSONString.data(using: .utf8) {
                    do {
                        responseDict = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    } catch {
                        print(error.localizedDescription)
                    }
                }
                let objSharedInstance = SharedClass.sharedInstance
                for i in 0...(responseDict.count-1)
                {
                    let jsonResponse = JSON(responseDict[i] as [String: Any])
                    satResultsApiResponse = SATResultsApiResponse(json: jsonResponse)
                    objSharedInstance.sharedInstanceForSATResults.append(satResultsApiResponse!)
                }
                
                
            }
            
        } catch {
        }
        
    }
    
    
    
    class func getSchoolDirectoryApiCall() -> NSArray
    {
        var responseInfo : NSArray?
        if let url = URL(string: SchoolDirectoryApiUrl)
        {
            do {
                let schoolDirectoryContents = try Data(contentsOf: url)
                
                guard let json = try? JSONSerialization.jsonObject(with: schoolDirectoryContents) else {
                    
                    print("There was an error!")
                    return responseInfo!
                }
                
                
                if json != nil
                {
                    responseInfo = json as? NSArray
                    
                }
                else
                {
                    
                }
                
            } catch {
            }
        } else {
            // the URL was bad!
        }
        
        return responseInfo!
        
    }
    
    class func getSATResultsApiCall() -> NSArray
    {
        var responseInfo : NSArray?
        if let url = URL(string: SATResultsApiUrl)
        {
            do {
                let siteModelDataContents = try Data(contentsOf: url)
                print(siteModelDataContents)
                
                //                let dataString = NSString(data: siteModelDataContents, encoding: String.Encoding.utf8.rawValue)
                
                //self.showSiteModelOldJSON()
                
                guard let json = try? JSONSerialization.jsonObject(with: siteModelDataContents) else {
                    
                    print("There was an error!")
                    return responseInfo!
                }
                
                
                if json != nil
                {
                    responseInfo = json as? NSArray
                    
                }
                else
                {
                    
                }
                
            } catch {
            }
        } else {
        }
        
        return responseInfo!
        
    }
    

    
}

