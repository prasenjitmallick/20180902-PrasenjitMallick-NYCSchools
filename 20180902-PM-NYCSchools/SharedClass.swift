//

//  Created by Prasenjit on 09/02/18.
//  Copyright © 2018 . All rights reserved.
//

import UIKit

class SharedClass: NSObject {
    
    class var sharedInstance: SharedClass {
        struct Static {
            static let instance = SharedClass()
        }
        return Static.instance
    }
    var sharedInstanceForSchoolDirectory = [SchoolDirectoryApiResponse]()
    var sharedInstanceForSATResults = [SATResultsApiResponse]()
}
