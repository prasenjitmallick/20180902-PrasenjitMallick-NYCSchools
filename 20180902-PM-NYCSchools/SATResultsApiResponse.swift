//
//  SATResultsApiResponse.swift
//
//  Created by Prasenjit on 09/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SATResultsApiResponse: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let schoolName = "school_name"
    static let satMathAvgScore = "sat_math_avg_score"
    static let satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
    static let satWritingAvgScore = "sat_writing_avg_score"
    static let numOfSatTestTakers = "num_of_sat_test_takers"
    static let dbn = "dbn"
  }

  // MARK: Properties
  public var schoolName: String?
  public var satMathAvgScore: String?
  public var satCriticalReadingAvgScore: String?
  public var satWritingAvgScore: String?
  public var numOfSatTestTakers: String?
  public var dbn: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    schoolName = json[SerializationKeys.schoolName].string
    satMathAvgScore = json[SerializationKeys.satMathAvgScore].string
    satCriticalReadingAvgScore = json[SerializationKeys.satCriticalReadingAvgScore].string
    satWritingAvgScore = json[SerializationKeys.satWritingAvgScore].string
    numOfSatTestTakers = json[SerializationKeys.numOfSatTestTakers].string
    dbn = json[SerializationKeys.dbn].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = schoolName { dictionary[SerializationKeys.schoolName] = value }
    if let value = satMathAvgScore { dictionary[SerializationKeys.satMathAvgScore] = value }
    if let value = satCriticalReadingAvgScore { dictionary[SerializationKeys.satCriticalReadingAvgScore] = value }
    if let value = satWritingAvgScore { dictionary[SerializationKeys.satWritingAvgScore] = value }
    if let value = numOfSatTestTakers { dictionary[SerializationKeys.numOfSatTestTakers] = value }
    if let value = dbn { dictionary[SerializationKeys.dbn] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.schoolName = aDecoder.decodeObject(forKey: SerializationKeys.schoolName) as? String
    self.satMathAvgScore = aDecoder.decodeObject(forKey: SerializationKeys.satMathAvgScore) as? String
    self.satCriticalReadingAvgScore = aDecoder.decodeObject(forKey: SerializationKeys.satCriticalReadingAvgScore) as? String
    self.satWritingAvgScore = aDecoder.decodeObject(forKey: SerializationKeys.satWritingAvgScore) as? String
    self.numOfSatTestTakers = aDecoder.decodeObject(forKey: SerializationKeys.numOfSatTestTakers) as? String
    self.dbn = aDecoder.decodeObject(forKey: SerializationKeys.dbn) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(schoolName, forKey: SerializationKeys.schoolName)
    aCoder.encode(satMathAvgScore, forKey: SerializationKeys.satMathAvgScore)
    aCoder.encode(satCriticalReadingAvgScore, forKey: SerializationKeys.satCriticalReadingAvgScore)
    aCoder.encode(satWritingAvgScore, forKey: SerializationKeys.satWritingAvgScore)
    aCoder.encode(numOfSatTestTakers, forKey: SerializationKeys.numOfSatTestTakers)
    aCoder.encode(dbn, forKey: SerializationKeys.dbn)
  }

}
