//
//  SchoolDetailsVC.swift
//
//  Created by Prasenjit on 2/9/18.
//  Copyright © 2018 . All rights reserved.
//

import UIKit

class SchoolDetailsVC: UIViewController {

    var detailsScreenDBN : String?
    let objSharedInstance = SharedClass.sharedInstance

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        for i in 0...(objSharedInstance.sharedInstanceForSATResults.count - 1)
        {
            
            if(objSharedInstance.sharedInstanceForSATResults[i].dbn == detailsScreenDBN)
            {
                schoolNameLabel.text = objSharedInstance.sharedInstanceForSATResults[i].schoolName
                testTakersLabel.text = "Num of SAT Test Takers: " + objSharedInstance.sharedInstanceForSATResults[i].numOfSatTestTakers!
                readingScoreLabel.text = "SAT Critical Reading Avg. Score: " +  objSharedInstance.sharedInstanceForSATResults[i].satCriticalReadingAvgScore!
                mathScoreLabel.text = "SAT Math Avg. Score: " +  objSharedInstance.sharedInstanceForSATResults[i].satMathAvgScore!
                writingScoreLabel.text = "SAT Writing Avg. Score: " +  objSharedInstance.sharedInstanceForSATResults[i].satWritingAvgScore!
                
                break
            }
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
