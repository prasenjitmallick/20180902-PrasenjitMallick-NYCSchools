//
//  ViewController.swift
//
//  Created by Prasenjit on 09/02/18.
//  Copyright © 2018 . All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let objSharedInstance = SharedClass.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        APIManagerClass.getSchoolDirectoryData()
        APIManagerClass.getSATResultsData()
        
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objSharedInstance.sharedInstanceForSchoolDirectory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolNameCell", for: indexPath) as! SchoolNameCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.nameLabel.text = objSharedInstance.sharedInstanceForSchoolDirectory[indexPath.row].schoolName
        cell.addressLabel.text = objSharedInstance.sharedInstanceForSchoolDirectory[indexPath.row].website
        cell.phoneNumberLable.text = objSharedInstance.sharedInstanceForSchoolDirectory[indexPath.row].phoneNumber
        cell.emailAddressLabel.text = objSharedInstance.sharedInstanceForSchoolDirectory[indexPath.row].schoolEmail
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolDetailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SchoolDetailsVC") as! SchoolDetailsVC
        schoolDetailsVC.detailsScreenDBN = objSharedInstance.sharedInstanceForSchoolDirectory[indexPath.row].dbn
        self.navigationController?.pushViewController(schoolDetailsVC, animated: true)
    }
    
    
}
