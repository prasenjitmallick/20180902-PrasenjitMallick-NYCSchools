//
//  SchoolNameCell.swift
//
//  Created by Prasenjit on 1/19/18.
//  Copyright © . All rights reserved.
//

import UIKit

class SchoolNameCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLable: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
