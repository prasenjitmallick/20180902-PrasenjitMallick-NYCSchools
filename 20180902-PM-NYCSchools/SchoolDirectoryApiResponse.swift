//
//  SchoolDirectoryApiResponse.swift
//
//  Created by Prasenjit on 09/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SchoolDirectoryApiResponse: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let grade9swdapplicantsperseat6 = "grade9swdapplicantsperseat6"
    static let requirement32 = "requirement3_2"
    static let seats103 = "seats103"
    static let grade9geapplicants7 = "grade9geapplicants7"
    static let directions4 = "directions4"
    static let interest2 = "interest2"
    static let eligibility3 = "eligibility3"
    static let code1 = "code1"
    static let bbl = "bbl"
    static let interest5 = "interest5"
    static let admissionspriority25 = "admissionspriority25"
    static let method7 = "method7"
    static let seats106 = "seats106"
    static let latitude = "latitude"
    static let grade9geapplicants8 = "grade9geapplicants8"
    static let admissionspriority15 = "admissionspriority15"
    static let prgdesc7 = "prgdesc7"
    static let eligibility1 = "eligibility1"
    static let grade9swdapplicants3 = "grade9swdapplicants3"
    static let code8 = "code8"
    static let commonAudition7 = "common_audition7"
    static let offerRate2 = "offer_rate2"
    static let grade9swdfilledflag6 = "grade9swdfilledflag6"
    static let grade9gefilledflag1 = "grade9gefilledflag1"
    static let admissionspriority17 = "admissionspriority17"
    static let admissionspriority53 = "admissionspriority53"
    static let admissionspriority41 = "admissionspriority41"
    static let grade9gefilledflag6 = "grade9gefilledflag6"
    static let city = "city"
    static let grade9geapplicantsperseat2 = "grade9geapplicantsperseat2"
    static let seats9swd3 = "seats9swd3"
    static let admissionspriority56 = "admissionspriority56"
    static let eligibility2 = "eligibility2"
    static let program7 = "program7"
    static let seats9ge7 = "seats9ge7"
    static let program4 = "program4"
    static let commonAudition1 = "common_audition1"
    static let method9 = "method9"
    static let auditioninformation4 = "auditioninformation4"
    static let applicants4specialized = "applicants4specialized"
    static let requirement15 = "requirement1_5"
    static let grade9swdfilledflag10 = "grade9swdfilledflag10"
    static let admissionspriority62 = "admissionspriority62"
    static let seats9swd8 = "seats9swd8"
    static let method3 = "method3"
    static let grade9swdfilledflag2 = "grade9swdfilledflag2"
    static let grade9swdapplicants5 = "grade9swdapplicants5"
    static let offerRate6 = "offer_rate6"
    static let admissionspriority33 = "admissionspriority33"
    static let primaryAddressLine1 = "primary_address_line_1"
    static let phoneNumber = "phone_number"
    static let grade9geapplicantsperseat5 = "grade9geapplicantsperseat5"
    static let seats4specialized = "seats4specialized"
    static let overviewParagraph = "overview_paragraph"
    static let program6 = "program6"
    static let grade9gefilledflag5 = "grade9gefilledflag5"
    static let grades2018 = "grades2018"
    static let seats6specialized = "seats6specialized"
    static let bin = "bin"
    static let psalSportsBoys = "psal_sports_boys"
    static let grade9gefilledflag4 = "grade9gefilledflag4"
    static let communityBoard = "community_board"
    static let grade9gefilledflag7 = "grade9gefilledflag7"
    static let appperseat1specialized = "appperseat1specialized"
    static let requirement51 = "requirement5_1"
    static let admissionspriority64 = "admissionspriority64"
    static let auditioninformation5 = "auditioninformation5"
    static let grade9geapplicantsperseat8 = "grade9geapplicantsperseat8"
    static let psalSportsGirls = "psal_sports_girls"
    static let grade9geapplicants4 = "grade9geapplicants4"
    static let grade9swdfilledflag1 = "grade9swdfilledflag1"
    static let directions6 = "directions6"
    static let admissionspriority35 = "admissionspriority35"
    static let dbn = "dbn"
    static let grade9geapplicantsperseat7 = "grade9geapplicantsperseat7"
    static let grade9swdapplicants7 = "grade9swdapplicants7"
    static let requirement21 = "requirement2_1"
    static let code4 = "code4"
    static let requirement44 = "requirement4_4"
    static let seats109 = "seats109"
    static let program9 = "program9"
    static let seats104 = "seats104"
    static let grade9swdfilledflag7 = "grade9swdfilledflag7"
    static let directions1 = "directions1"
    static let schoolEmail = "school_email"
    static let requirement62 = "requirement6_2"
    static let requirement16 = "requirement1_6"
    static let requirement23 = "requirement2_3"
    static let code2 = "code2"
    static let international = "international"
    static let applicants5specialized = "applicants5specialized"
    static let requirement42 = "requirement4_2"
    static let appperseat3specialized = "appperseat3specialized"
    static let schoolName = "school_name"
    static let collegeCareerRate = "college_career_rate"
    static let applicants6specialized = "applicants6specialized"
    static let seats9swd7 = "seats9swd7"
    static let grade9geapplicantsperseat1 = "grade9geapplicantsperseat1"
    static let borough = "borough"
    static let admissionspriority54 = "admissionspriority54"
    static let grade9swdapplicants10 = "grade9swdapplicants10"
    static let grade9geapplicants1 = "grade9geapplicants1"
    static let seats9ge6 = "seats9ge6"
    static let interest4 = "interest4"
    static let method10 = "method10"
    static let requirement46 = "requirement4_6"
    static let program1 = "program1"
    static let admissionspriority52 = "admissionspriority52"
    static let neighborhood = "neighborhood"
    static let auditioninformation1 = "auditioninformation1"
    static let grade9geapplicantsperseat6 = "grade9geapplicantsperseat6"
    static let prgdesc3 = "prgdesc3"
    static let seats2specialized = "seats2specialized"
    static let requirement57 = "requirement5_7"
    static let seats9swd10 = "seats9swd10"
    static let grade9swdapplicants2 = "grade9swdapplicants2"
    static let earlycollege = "earlycollege"
    static let admissionspriority12 = "admissionspriority12"
    static let admissionspriority19 = "admissionspriority19"
    static let code5 = "code5"
    static let directions2 = "directions2"
    static let code7 = "code7"
    static let seats9ge8 = "seats9ge8"
    static let grade9swdapplicantsperseat9 = "grade9swdapplicantsperseat9"
    static let schoolSports = "school_sports"
    static let requirement13 = "requirement1_3"
    static let grade9geapplicantsperseat9 = "grade9geapplicantsperseat9"
    static let diplomaendorsements = "diplomaendorsements"
    static let interest1 = "interest1"
    static let auditioninformation6 = "auditioninformation6"
    static let schoolAccessibilityDescription = "school_accessibility_description"
    static let grade9geapplicants3 = "grade9geapplicants3"
    static let grade9swdfilledflag9 = "grade9swdfilledflag9"
    static let program3 = "program3"
    static let subway = "subway"
    static let grade9swdapplicants4 = "grade9swdapplicants4"
    static let program2 = "program2"
    static let program10 = "program10"
    static let offerRate3 = "offer_rate3"
    static let offerRate5 = "offer_rate5"
    static let applicants2specialized = "applicants2specialized"
    static let admissionspriority16 = "admissionspriority16"
    static let grade9geapplicantsperseat10 = "grade9geapplicantsperseat10"
    static let offerRate8 = "offer_rate8"
    static let sharedSpace = "shared_space"
    static let requirement55 = "requirement5_5"
    static let method6 = "method6"
    static let admissionspriority18 = "admissionspriority18"
    static let seats9ge9 = "seats9ge9"
    static let seats9swd6 = "seats9swd6"
    static let auditioninformation3 = "auditioninformation3"
    static let grade9swdapplicants6 = "grade9swdapplicants6"
    static let admissionspriority26 = "admissionspriority26"
    static let transfer = "transfer"
    static let requirement33 = "requirement3_3"
    static let directions3 = "directions3"
    static let graduationRate = "graduation_rate"
    static let location = "location"
    static let pctStuEnoughVariety = "pct_stu_enough_variety"
    static let prgdesc9 = "prgdesc9"
    static let prgdesc10 = "prgdesc10"
    static let admissionspriority110 = "admissionspriority110"
    static let languageClasses = "language_classes"
    static let commonAudition2 = "common_audition2"
    static let applicants3specialized = "applicants3specialized"
    static let admissionspriority51 = "admissionspriority51"
    static let seats9ge5 = "seats9ge5"
    static let requirement37 = "requirement3_7"
    static let requirement36 = "requirement3_6"
    static let academicopportunities1 = "academicopportunities1"
    static let pctStuSafe = "pct_stu_safe"
    static let bus = "bus"
    static let requirement12 = "requirement1_2"
    static let requirement53 = "requirement5_3"
    static let stateCode = "state_code"
    static let grade9geapplicants5 = "grade9geapplicants5"
    static let seats9swd4 = "seats9swd4"
    static let code6 = "code6"
    static let code9 = "code9"
    static let grade9swdfilledflag4 = "grade9swdfilledflag4"
    static let grade9swdapplicantsperseat3 = "grade9swdapplicantsperseat3"
    static let prgdesc5 = "prgdesc5"
    static let method2 = "method2"
    static let admissionspriority22 = "admissionspriority22"
    static let directions7 = "directions7"
    static let admissionspriority46 = "admissionspriority46"
    static let offerRate9 = "offer_rate9"
    static let seats107 = "seats107"
    static let academicopportunities2 = "academicopportunities2"
    static let grade9swdapplicantsperseat2 = "grade9swdapplicantsperseat2"
    static let seats102 = "seats102"
    static let requirement24 = "requirement2_4"
    static let method1 = "method1"
    static let commonAudition4 = "common_audition4"
    static let prgdesc8 = "prgdesc8"
    static let grade9geapplicants10 = "grade9geapplicants10"
    static let requirement31 = "requirement3_1"
    static let grade9swdapplicants9 = "grade9swdapplicants9"
    static let requirement18 = "requirement1_8"
    static let grade9geapplicants9 = "grade9geapplicants9"
    static let website = "website"
    static let interest3 = "interest3"
    static let commonAudition6 = "common_audition6"
    static let requirement54 = "requirement5_4"
    static let method8 = "method8"
    static let admissionspriority36 = "admissionspriority36"
    static let grade9gefilledflag9 = "grade9gefilledflag9"
    static let grade9geapplicantsperseat3 = "grade9geapplicantsperseat3"
    static let grade9gefilledflag10 = "grade9gefilledflag10"
    static let admissionspriority44 = "admissionspriority44"
    static let boro = "boro"
    static let totalStudents = "total_students"
    static let interest10 = "interest10"
    static let grade9geapplicants2 = "grade9geapplicants2"
    static let ellPrograms = "ell_programs"
    static let auditioninformation2 = "auditioninformation2"
    static let interest7 = "interest7"
    static let requirement47 = "requirement4_7"
    static let admissionspriority63 = "admissionspriority63"
    static let grade9geapplicantsperseat4 = "grade9geapplicantsperseat4"
    static let admissionspriority32 = "admissionspriority32"
    static let requirement67 = "requirement6_7"
    static let seats9ge4 = "seats9ge4"
    static let finalgrades = "finalgrades"
    static let grade9gefilledflag3 = "grade9gefilledflag3"
    static let appperseat6specialized = "appperseat6specialized"
    static let interest6 = "interest6"
    static let seats9swd1 = "seats9swd1"
    static let grade9swdapplicantsperseat5 = "grade9swdapplicantsperseat5"
    static let offerRate1 = "offer_rate1"
    static let extracurricularActivities = "extracurricular_activities"
    static let zip = "zip"
    static let faxNumber = "fax_number"
    static let endTime = "end_time"
    static let requirement35 = "requirement3_5"
    static let admissionspriority13 = "admissionspriority13"
    static let admissionspriority37 = "admissionspriority37"
    static let auditioninformation7 = "auditioninformation7"
    static let longitude = "longitude"
    static let prgdesc4 = "prgdesc4"
    static let eligibility7 = "eligibility7"
    static let offerRate4 = "offer_rate4"
    static let grade9geapplicants6 = "grade9geapplicants6"
    static let grade9swdapplicantsperseat8 = "grade9swdapplicantsperseat8"
    static let seats108 = "seats108"
    static let admissionspriority74 = "admissionspriority74"
    static let seats3specialized = "seats3specialized"
    static let requirement11 = "requirement1_1"
    static let grade9gefilledflag2 = "grade9gefilledflag2"
    static let seats101 = "seats101"
    static let interest8 = "interest8"
    static let commonAudition5 = "common_audition5"
    static let appperseat5specialized = "appperseat5specialized"
    static let admissionspriority43 = "admissionspriority43"
    static let interest9 = "interest9"
    static let eligibility5 = "eligibility5"
    static let requirement34 = "requirement3_4"
    static let grade9swdapplicantsperseat10 = "grade9swdapplicantsperseat10"
    static let grade9swdfilledflag3 = "grade9swdfilledflag3"
    static let seats1010 = "seats1010"
    static let grade9swdfilledflag8 = "grade9swdfilledflag8"
    static let specialized = "specialized"
    static let grade9swdapplicants8 = "grade9swdapplicants8"
    static let appperseat2specialized = "appperseat2specialized"
    static let nta = "nta"
    static let academicopportunities3 = "academicopportunities3"
    static let geoeligibility = "geoeligibility"
    static let censusTract = "census_tract"
    static let requirement22 = "requirement2_2"
    static let pbat = "pbat"
    static let requirement38 = "requirement3_8"
    static let applicants1specialized = "applicants1specialized"
    static let eligibility4 = "eligibility4"
    static let prgdesc2 = "prgdesc2"
    static let girls = "girls"
    static let method5 = "method5"
    static let seats9swd5 = "seats9swd5"
    static let requirement26 = "requirement2_6"
    static let academicopportunities5 = "academicopportunities5"
    static let program8 = "program8"
    static let admissionspriority71 = "admissionspriority71"
    static let academicopportunities4 = "academicopportunities4"
    static let buildingCode = "building_code"
    static let councilDistrict = "council_district"
    static let seats1specialized = "seats1specialized"
    static let directions5 = "directions5"
    static let appperseat4specialized = "appperseat4specialized"
    static let admissionspriority28 = "admissionspriority28"
    static let requirement61 = "requirement6_1"
    static let grade9gefilledflag8 = "grade9gefilledflag8"
    static let admissionspriority23 = "admissionspriority23"
    static let seats105 = "seats105"
    static let attendanceRate = "attendance_rate"
    static let admissionspriority21 = "admissionspriority21"
    static let psalSportsCoed = "psal_sports_coed"
    static let commonAudition3 = "common_audition3"
    static let code10 = "code10"
    static let offerRate7 = "offer_rate7"
    static let seats9swd9 = "seats9swd9"
    static let seats9ge2 = "seats9ge2"
    static let requirement56 = "requirement5_6"
    static let admissionspriority14 = "admissionspriority14"
    static let prgdesc6 = "prgdesc6"
    static let grade9swdapplicants1 = "grade9swdapplicants1"
    static let program5 = "program5"
    static let requirement45 = "requirement4_5"
    static let admissionspriority27 = "admissionspriority27"
    static let seats5specialized = "seats5specialized"
    static let advancedplacementCourses = "advancedplacement_courses"
    static let admissionspriority42 = "admissionspriority42"
    static let grade9swdfilledflag5 = "grade9swdfilledflag5"
    static let admissionspriority34 = "admissionspriority34"
    static let seats9ge1 = "seats9ge1"
    static let requirement41 = "requirement4_1"
    static let requirement14 = "requirement1_4"
    static let grade9swdapplicantsperseat4 = "grade9swdapplicantsperseat4"
    static let requirement52 = "requirement5_2"
    static let admissionspriority31 = "admissionspriority31"
    static let addtlInfo1 = "addtl_info1"
    static let code3 = "code3"
    static let grade9swdapplicantsperseat7 = "grade9swdapplicantsperseat7"
    static let requirement63 = "requirement6_3"
    static let startTime = "start_time"
    static let eligibility6 = "eligibility6"
    static let seats9ge10 = "seats9ge10"
    static let requirement25 = "requirement2_5"
    static let requirement27 = "requirement2_7"
    static let school10thSeats = "school_10th_seats"
    static let admissionspriority61 = "admissionspriority61"
    static let seats9swd2 = "seats9swd2"
    static let ptech = "ptech"
    static let prgdesc1 = "prgdesc1"
    static let admissionspriority29 = "admissionspriority29"
    static let grade9swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
    static let seats9ge3 = "seats9ge3"
    static let admissionspriority11 = "admissionspriority11"
    static let campusName = "campus_name"
    static let admissionspriority24 = "admissionspriority24"
    static let requirement43 = "requirement4_3"
    static let boys = "boys"
    static let method4 = "method4"
    static let requirement17 = "requirement1_7"
    static let requirement28 = "requirement2_8"
  }

  // MARK: Properties
  public var grade9swdapplicantsperseat6: String?
  public var requirement32: String?
  public var seats103: String?
  public var grade9geapplicants7: String?
  public var directions4: String?
  public var interest2: String?
  public var eligibility3: String?
  public var code1: String?
  public var bbl: String?
  public var interest5: String?
  public var admissionspriority25: String?
  public var method7: String?
  public var seats106: String?
  public var latitude: String?
  public var grade9geapplicants8: String?
  public var admissionspriority15: String?
  public var prgdesc7: String?
  public var eligibility1: String?
  public var grade9swdapplicants3: String?
  public var code8: String?
  public var commonAudition7: String?
  public var offerRate2: String?
  public var grade9swdfilledflag6: String?
  public var grade9gefilledflag1: String?
  public var admissionspriority17: String?
  public var admissionspriority53: String?
  public var admissionspriority41: String?
  public var grade9gefilledflag6: String?
  public var city: String?
  public var grade9geapplicantsperseat2: String?
  public var seats9swd3: String?
  public var admissionspriority56: String?
  public var eligibility2: String?
  public var program7: String?
  public var seats9ge7: String?
  public var program4: String?
  public var commonAudition1: String?
  public var method9: String?
  public var auditioninformation4: String?
  public var applicants4specialized: String?
  public var requirement15: String?
  public var grade9swdfilledflag10: String?
  public var admissionspriority62: String?
  public var seats9swd8: String?
  public var method3: String?
  public var grade9swdfilledflag2: String?
  public var grade9swdapplicants5: String?
  public var offerRate6: String?
  public var admissionspriority33: String?
  public var primaryAddressLine1: String?
  public var phoneNumber: String?
  public var grade9geapplicantsperseat5: String?
  public var seats4specialized: String?
  public var overviewParagraph: String?
  public var program6: String?
  public var grade9gefilledflag5: String?
  public var grades2018: String?
  public var seats6specialized: String?
  public var bin: String?
  public var psalSportsBoys: String?
  public var grade9gefilledflag4: String?
  public var communityBoard: String?
  public var grade9gefilledflag7: String?
  public var appperseat1specialized: String?
  public var requirement51: String?
  public var admissionspriority64: String?
  public var auditioninformation5: String?
  public var grade9geapplicantsperseat8: String?
  public var psalSportsGirls: String?
  public var grade9geapplicants4: String?
  public var grade9swdfilledflag1: String?
  public var directions6: String?
  public var admissionspriority35: String?
  public var dbn: String?
  public var grade9geapplicantsperseat7: String?
  public var grade9swdapplicants7: String?
  public var requirement21: String?
  public var code4: String?
  public var requirement44: String?
  public var seats109: String?
  public var program9: String?
  public var seats104: String?
  public var grade9swdfilledflag7: String?
  public var directions1: String?
  public var schoolEmail: String?
  public var requirement62: String?
  public var requirement16: String?
  public var requirement23: String?
  public var code2: String?
  public var international: String?
  public var applicants5specialized: String?
  public var requirement42: String?
  public var appperseat3specialized: String?
  public var schoolName: String?
  public var collegeCareerRate: String?
  public var applicants6specialized: String?
  public var seats9swd7: String?
  public var grade9geapplicantsperseat1: String?
  public var borough: String?
  public var admissionspriority54: String?
  public var grade9swdapplicants10: String?
  public var grade9geapplicants1: String?
  public var seats9ge6: String?
  public var interest4: String?
  public var method10: String?
  public var requirement46: String?
  public var program1: String?
  public var admissionspriority52: String?
  public var neighborhood: String?
  public var auditioninformation1: String?
  public var grade9geapplicantsperseat6: String?
  public var prgdesc3: String?
  public var seats2specialized: String?
  public var requirement57: String?
  public var seats9swd10: String?
  public var grade9swdapplicants2: String?
  public var earlycollege: String?
  public var admissionspriority12: String?
  public var admissionspriority19: String?
  public var code5: String?
  public var directions2: String?
  public var code7: String?
  public var seats9ge8: String?
  public var grade9swdapplicantsperseat9: String?
  public var schoolSports: String?
  public var requirement13: String?
  public var grade9geapplicantsperseat9: String?
  public var diplomaendorsements: String?
  public var interest1: String?
  public var auditioninformation6: String?
  public var schoolAccessibilityDescription: String?
  public var grade9geapplicants3: String?
  public var grade9swdfilledflag9: String?
  public var program3: String?
  public var subway: String?
  public var grade9swdapplicants4: String?
  public var program2: String?
  public var program10: String?
  public var offerRate3: String?
  public var offerRate5: String?
  public var applicants2specialized: String?
  public var admissionspriority16: String?
  public var grade9geapplicantsperseat10: String?
  public var offerRate8: String?
  public var sharedSpace: String?
  public var requirement55: String?
  public var method6: String?
  public var admissionspriority18: String?
  public var seats9ge9: String?
  public var seats9swd6: String?
  public var auditioninformation3: String?
  public var grade9swdapplicants6: String?
  public var admissionspriority26: String?
  public var transfer: String?
  public var requirement33: String?
  public var directions3: String?
  public var graduationRate: String?
  public var location: String?
  public var pctStuEnoughVariety: String?
  public var prgdesc9: String?
  public var prgdesc10: String?
  public var admissionspriority110: String?
  public var languageClasses: String?
  public var commonAudition2: String?
  public var applicants3specialized: String?
  public var admissionspriority51: String?
  public var seats9ge5: String?
  public var requirement37: String?
  public var requirement36: String?
  public var academicopportunities1: String?
  public var pctStuSafe: String?
  public var bus: String?
  public var requirement12: String?
  public var requirement53: String?
  public var stateCode: String?
  public var grade9geapplicants5: String?
  public var seats9swd4: String?
  public var code6: String?
  public var code9: String?
  public var grade9swdfilledflag4: String?
  public var grade9swdapplicantsperseat3: String?
  public var prgdesc5: String?
  public var method2: String?
  public var admissionspriority22: String?
  public var directions7: String?
  public var admissionspriority46: String?
  public var offerRate9: String?
  public var seats107: String?
  public var academicopportunities2: String?
  public var grade9swdapplicantsperseat2: String?
  public var seats102: String?
  public var requirement24: String?
  public var method1: String?
  public var commonAudition4: String?
  public var prgdesc8: String?
  public var grade9geapplicants10: String?
  public var requirement31: String?
  public var grade9swdapplicants9: String?
  public var requirement18: String?
  public var grade9geapplicants9: String?
  public var website: String?
  public var interest3: String?
  public var commonAudition6: String?
  public var requirement54: String?
  public var method8: String?
  public var admissionspriority36: String?
  public var grade9gefilledflag9: String?
  public var grade9geapplicantsperseat3: String?
  public var grade9gefilledflag10: String?
  public var admissionspriority44: String?
  public var boro: String?
  public var totalStudents: String?
  public var interest10: String?
  public var grade9geapplicants2: String?
  public var ellPrograms: String?
  public var auditioninformation2: String?
  public var interest7: String?
  public var requirement47: String?
  public var admissionspriority63: String?
  public var grade9geapplicantsperseat4: String?
  public var admissionspriority32: String?
  public var requirement67: String?
  public var seats9ge4: String?
  public var finalgrades: String?
  public var grade9gefilledflag3: String?
  public var appperseat6specialized: String?
  public var interest6: String?
  public var seats9swd1: String?
  public var grade9swdapplicantsperseat5: String?
  public var offerRate1: String?
  public var extracurricularActivities: String?
  public var zip: String?
  public var faxNumber: String?
  public var endTime: String?
  public var requirement35: String?
  public var admissionspriority13: String?
  public var admissionspriority37: String?
  public var auditioninformation7: String?
  public var longitude: String?
  public var prgdesc4: String?
  public var eligibility7: String?
  public var offerRate4: String?
  public var grade9geapplicants6: String?
  public var grade9swdapplicantsperseat8: String?
  public var seats108: String?
  public var admissionspriority74: String?
  public var seats3specialized: String?
  public var requirement11: String?
  public var grade9gefilledflag2: String?
  public var seats101: String?
  public var interest8: String?
  public var commonAudition5: String?
  public var appperseat5specialized: String?
  public var admissionspriority43: String?
  public var interest9: String?
  public var eligibility5: String?
  public var requirement34: String?
  public var grade9swdapplicantsperseat10: String?
  public var grade9swdfilledflag3: String?
  public var seats1010: String?
  public var grade9swdfilledflag8: String?
  public var specialized: String?
  public var grade9swdapplicants8: String?
  public var appperseat2specialized: String?
  public var nta: String?
  public var academicopportunities3: String?
  public var geoeligibility: String?
  public var censusTract: String?
  public var requirement22: String?
  public var pbat: String?
  public var requirement38: String?
  public var applicants1specialized: String?
  public var eligibility4: String?
  public var prgdesc2: String?
  public var girls: String?
  public var method5: String?
  public var seats9swd5: String?
  public var requirement26: String?
  public var academicopportunities5: String?
  public var program8: String?
  public var admissionspriority71: String?
  public var academicopportunities4: String?
  public var buildingCode: String?
  public var councilDistrict: String?
  public var seats1specialized: String?
  public var directions5: String?
  public var appperseat4specialized: String?
  public var admissionspriority28: String?
  public var requirement61: String?
  public var grade9gefilledflag8: String?
  public var admissionspriority23: String?
  public var seats105: String?
  public var attendanceRate: String?
  public var admissionspriority21: String?
  public var psalSportsCoed: String?
  public var commonAudition3: String?
  public var code10: String?
  public var offerRate7: String?
  public var seats9swd9: String?
  public var seats9ge2: String?
  public var requirement56: String?
  public var admissionspriority14: String?
  public var prgdesc6: String?
  public var grade9swdapplicants1: String?
  public var program5: String?
  public var requirement45: String?
  public var admissionspriority27: String?
  public var seats5specialized: String?
  public var advancedplacementCourses: String?
  public var admissionspriority42: String?
  public var grade9swdfilledflag5: String?
  public var admissionspriority34: String?
  public var seats9ge1: String?
  public var requirement41: String?
  public var requirement14: String?
  public var grade9swdapplicantsperseat4: String?
  public var requirement52: String?
  public var admissionspriority31: String?
  public var addtlInfo1: String?
  public var code3: String?
  public var grade9swdapplicantsperseat7: String?
  public var requirement63: String?
  public var startTime: String?
  public var eligibility6: String?
  public var seats9ge10: String?
  public var requirement25: String?
  public var requirement27: String?
  public var school10thSeats: String?
  public var admissionspriority61: String?
  public var seats9swd2: String?
  public var ptech: String?
  public var prgdesc1: String?
  public var admissionspriority29: String?
  public var grade9swdapplicantsperseat1: String?
  public var seats9ge3: String?
  public var admissionspriority11: String?
  public var campusName: String?
  public var admissionspriority24: String?
  public var requirement43: String?
  public var boys: String?
  public var method4: String?
  public var requirement17: String?
  public var requirement28: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    grade9swdapplicantsperseat6 = json[SerializationKeys.grade9swdapplicantsperseat6].string
    requirement32 = json[SerializationKeys.requirement32].string
    seats103 = json[SerializationKeys.seats103].string
    grade9geapplicants7 = json[SerializationKeys.grade9geapplicants7].string
    directions4 = json[SerializationKeys.directions4].string
    interest2 = json[SerializationKeys.interest2].string
    eligibility3 = json[SerializationKeys.eligibility3].string
    code1 = json[SerializationKeys.code1].string
    bbl = json[SerializationKeys.bbl].string
    interest5 = json[SerializationKeys.interest5].string
    admissionspriority25 = json[SerializationKeys.admissionspriority25].string
    method7 = json[SerializationKeys.method7].string
    seats106 = json[SerializationKeys.seats106].string
    latitude = json[SerializationKeys.latitude].string
    grade9geapplicants8 = json[SerializationKeys.grade9geapplicants8].string
    admissionspriority15 = json[SerializationKeys.admissionspriority15].string
    prgdesc7 = json[SerializationKeys.prgdesc7].string
    eligibility1 = json[SerializationKeys.eligibility1].string
    grade9swdapplicants3 = json[SerializationKeys.grade9swdapplicants3].string
    code8 = json[SerializationKeys.code8].string
    commonAudition7 = json[SerializationKeys.commonAudition7].string
    offerRate2 = json[SerializationKeys.offerRate2].string
    grade9swdfilledflag6 = json[SerializationKeys.grade9swdfilledflag6].string
    grade9gefilledflag1 = json[SerializationKeys.grade9gefilledflag1].string
    admissionspriority17 = json[SerializationKeys.admissionspriority17].string
    admissionspriority53 = json[SerializationKeys.admissionspriority53].string
    admissionspriority41 = json[SerializationKeys.admissionspriority41].string
    grade9gefilledflag6 = json[SerializationKeys.grade9gefilledflag6].string
    city = json[SerializationKeys.city].string
    grade9geapplicantsperseat2 = json[SerializationKeys.grade9geapplicantsperseat2].string
    seats9swd3 = json[SerializationKeys.seats9swd3].string
    admissionspriority56 = json[SerializationKeys.admissionspriority56].string
    eligibility2 = json[SerializationKeys.eligibility2].string
    program7 = json[SerializationKeys.program7].string
    seats9ge7 = json[SerializationKeys.seats9ge7].string
    program4 = json[SerializationKeys.program4].string
    commonAudition1 = json[SerializationKeys.commonAudition1].string
    method9 = json[SerializationKeys.method9].string
    auditioninformation4 = json[SerializationKeys.auditioninformation4].string
    applicants4specialized = json[SerializationKeys.applicants4specialized].string
    requirement15 = json[SerializationKeys.requirement15].string
    grade9swdfilledflag10 = json[SerializationKeys.grade9swdfilledflag10].string
    admissionspriority62 = json[SerializationKeys.admissionspriority62].string
    seats9swd8 = json[SerializationKeys.seats9swd8].string
    method3 = json[SerializationKeys.method3].string
    grade9swdfilledflag2 = json[SerializationKeys.grade9swdfilledflag2].string
    grade9swdapplicants5 = json[SerializationKeys.grade9swdapplicants5].string
    offerRate6 = json[SerializationKeys.offerRate6].string
    admissionspriority33 = json[SerializationKeys.admissionspriority33].string
    primaryAddressLine1 = json[SerializationKeys.primaryAddressLine1].string
    phoneNumber = json[SerializationKeys.phoneNumber].string
    grade9geapplicantsperseat5 = json[SerializationKeys.grade9geapplicantsperseat5].string
    seats4specialized = json[SerializationKeys.seats4specialized].string
    overviewParagraph = json[SerializationKeys.overviewParagraph].string
    program6 = json[SerializationKeys.program6].string
    grade9gefilledflag5 = json[SerializationKeys.grade9gefilledflag5].string
    grades2018 = json[SerializationKeys.grades2018].string
    seats6specialized = json[SerializationKeys.seats6specialized].string
    bin = json[SerializationKeys.bin].string
    psalSportsBoys = json[SerializationKeys.psalSportsBoys].string
    grade9gefilledflag4 = json[SerializationKeys.grade9gefilledflag4].string
    communityBoard = json[SerializationKeys.communityBoard].string
    grade9gefilledflag7 = json[SerializationKeys.grade9gefilledflag7].string
    appperseat1specialized = json[SerializationKeys.appperseat1specialized].string
    requirement51 = json[SerializationKeys.requirement51].string
    admissionspriority64 = json[SerializationKeys.admissionspriority64].string
    auditioninformation5 = json[SerializationKeys.auditioninformation5].string
    grade9geapplicantsperseat8 = json[SerializationKeys.grade9geapplicantsperseat8].string
    psalSportsGirls = json[SerializationKeys.psalSportsGirls].string
    grade9geapplicants4 = json[SerializationKeys.grade9geapplicants4].string
    grade9swdfilledflag1 = json[SerializationKeys.grade9swdfilledflag1].string
    directions6 = json[SerializationKeys.directions6].string
    admissionspriority35 = json[SerializationKeys.admissionspriority35].string
    dbn = json[SerializationKeys.dbn].string
    grade9geapplicantsperseat7 = json[SerializationKeys.grade9geapplicantsperseat7].string
    grade9swdapplicants7 = json[SerializationKeys.grade9swdapplicants7].string
    requirement21 = json[SerializationKeys.requirement21].string
    code4 = json[SerializationKeys.code4].string
    requirement44 = json[SerializationKeys.requirement44].string
    seats109 = json[SerializationKeys.seats109].string
    program9 = json[SerializationKeys.program9].string
    seats104 = json[SerializationKeys.seats104].string
    grade9swdfilledflag7 = json[SerializationKeys.grade9swdfilledflag7].string
    directions1 = json[SerializationKeys.directions1].string
    schoolEmail = json[SerializationKeys.schoolEmail].string
    requirement62 = json[SerializationKeys.requirement62].string
    requirement16 = json[SerializationKeys.requirement16].string
    requirement23 = json[SerializationKeys.requirement23].string
    code2 = json[SerializationKeys.code2].string
    international = json[SerializationKeys.international].string
    applicants5specialized = json[SerializationKeys.applicants5specialized].string
    requirement42 = json[SerializationKeys.requirement42].string
    appperseat3specialized = json[SerializationKeys.appperseat3specialized].string
    schoolName = json[SerializationKeys.schoolName].string
    collegeCareerRate = json[SerializationKeys.collegeCareerRate].string
    applicants6specialized = json[SerializationKeys.applicants6specialized].string
    seats9swd7 = json[SerializationKeys.seats9swd7].string
    grade9geapplicantsperseat1 = json[SerializationKeys.grade9geapplicantsperseat1].string
    borough = json[SerializationKeys.borough].string
    admissionspriority54 = json[SerializationKeys.admissionspriority54].string
    grade9swdapplicants10 = json[SerializationKeys.grade9swdapplicants10].string
    grade9geapplicants1 = json[SerializationKeys.grade9geapplicants1].string
    seats9ge6 = json[SerializationKeys.seats9ge6].string
    interest4 = json[SerializationKeys.interest4].string
    method10 = json[SerializationKeys.method10].string
    requirement46 = json[SerializationKeys.requirement46].string
    program1 = json[SerializationKeys.program1].string
    admissionspriority52 = json[SerializationKeys.admissionspriority52].string
    neighborhood = json[SerializationKeys.neighborhood].string
    auditioninformation1 = json[SerializationKeys.auditioninformation1].string
    grade9geapplicantsperseat6 = json[SerializationKeys.grade9geapplicantsperseat6].string
    prgdesc3 = json[SerializationKeys.prgdesc3].string
    seats2specialized = json[SerializationKeys.seats2specialized].string
    requirement57 = json[SerializationKeys.requirement57].string
    seats9swd10 = json[SerializationKeys.seats9swd10].string
    grade9swdapplicants2 = json[SerializationKeys.grade9swdapplicants2].string
    earlycollege = json[SerializationKeys.earlycollege].string
    admissionspriority12 = json[SerializationKeys.admissionspriority12].string
    admissionspriority19 = json[SerializationKeys.admissionspriority19].string
    code5 = json[SerializationKeys.code5].string
    directions2 = json[SerializationKeys.directions2].string
    code7 = json[SerializationKeys.code7].string
    seats9ge8 = json[SerializationKeys.seats9ge8].string
    grade9swdapplicantsperseat9 = json[SerializationKeys.grade9swdapplicantsperseat9].string
    schoolSports = json[SerializationKeys.schoolSports].string
    requirement13 = json[SerializationKeys.requirement13].string
    grade9geapplicantsperseat9 = json[SerializationKeys.grade9geapplicantsperseat9].string
    diplomaendorsements = json[SerializationKeys.diplomaendorsements].string
    interest1 = json[SerializationKeys.interest1].string
    auditioninformation6 = json[SerializationKeys.auditioninformation6].string
    schoolAccessibilityDescription = json[SerializationKeys.schoolAccessibilityDescription].string
    grade9geapplicants3 = json[SerializationKeys.grade9geapplicants3].string
    grade9swdfilledflag9 = json[SerializationKeys.grade9swdfilledflag9].string
    program3 = json[SerializationKeys.program3].string
    subway = json[SerializationKeys.subway].string
    grade9swdapplicants4 = json[SerializationKeys.grade9swdapplicants4].string
    program2 = json[SerializationKeys.program2].string
    program10 = json[SerializationKeys.program10].string
    offerRate3 = json[SerializationKeys.offerRate3].string
    offerRate5 = json[SerializationKeys.offerRate5].string
    applicants2specialized = json[SerializationKeys.applicants2specialized].string
    admissionspriority16 = json[SerializationKeys.admissionspriority16].string
    grade9geapplicantsperseat10 = json[SerializationKeys.grade9geapplicantsperseat10].string
    offerRate8 = json[SerializationKeys.offerRate8].string
    sharedSpace = json[SerializationKeys.sharedSpace].string
    requirement55 = json[SerializationKeys.requirement55].string
    method6 = json[SerializationKeys.method6].string
    admissionspriority18 = json[SerializationKeys.admissionspriority18].string
    seats9ge9 = json[SerializationKeys.seats9ge9].string
    seats9swd6 = json[SerializationKeys.seats9swd6].string
    auditioninformation3 = json[SerializationKeys.auditioninformation3].string
    grade9swdapplicants6 = json[SerializationKeys.grade9swdapplicants6].string
    admissionspriority26 = json[SerializationKeys.admissionspriority26].string
    transfer = json[SerializationKeys.transfer].string
    requirement33 = json[SerializationKeys.requirement33].string
    directions3 = json[SerializationKeys.directions3].string
    graduationRate = json[SerializationKeys.graduationRate].string
    location = json[SerializationKeys.location].string
    pctStuEnoughVariety = json[SerializationKeys.pctStuEnoughVariety].string
    prgdesc9 = json[SerializationKeys.prgdesc9].string
    prgdesc10 = json[SerializationKeys.prgdesc10].string
    admissionspriority110 = json[SerializationKeys.admissionspriority110].string
    languageClasses = json[SerializationKeys.languageClasses].string
    commonAudition2 = json[SerializationKeys.commonAudition2].string
    applicants3specialized = json[SerializationKeys.applicants3specialized].string
    admissionspriority51 = json[SerializationKeys.admissionspriority51].string
    seats9ge5 = json[SerializationKeys.seats9ge5].string
    requirement37 = json[SerializationKeys.requirement37].string
    requirement36 = json[SerializationKeys.requirement36].string
    academicopportunities1 = json[SerializationKeys.academicopportunities1].string
    pctStuSafe = json[SerializationKeys.pctStuSafe].string
    bus = json[SerializationKeys.bus].string
    requirement12 = json[SerializationKeys.requirement12].string
    requirement53 = json[SerializationKeys.requirement53].string
    stateCode = json[SerializationKeys.stateCode].string
    grade9geapplicants5 = json[SerializationKeys.grade9geapplicants5].string
    seats9swd4 = json[SerializationKeys.seats9swd4].string
    code6 = json[SerializationKeys.code6].string
    code9 = json[SerializationKeys.code9].string
    grade9swdfilledflag4 = json[SerializationKeys.grade9swdfilledflag4].string
    grade9swdapplicantsperseat3 = json[SerializationKeys.grade9swdapplicantsperseat3].string
    prgdesc5 = json[SerializationKeys.prgdesc5].string
    method2 = json[SerializationKeys.method2].string
    admissionspriority22 = json[SerializationKeys.admissionspriority22].string
    directions7 = json[SerializationKeys.directions7].string
    admissionspriority46 = json[SerializationKeys.admissionspriority46].string
    offerRate9 = json[SerializationKeys.offerRate9].string
    seats107 = json[SerializationKeys.seats107].string
    academicopportunities2 = json[SerializationKeys.academicopportunities2].string
    grade9swdapplicantsperseat2 = json[SerializationKeys.grade9swdapplicantsperseat2].string
    seats102 = json[SerializationKeys.seats102].string
    requirement24 = json[SerializationKeys.requirement24].string
    method1 = json[SerializationKeys.method1].string
    commonAudition4 = json[SerializationKeys.commonAudition4].string
    prgdesc8 = json[SerializationKeys.prgdesc8].string
    grade9geapplicants10 = json[SerializationKeys.grade9geapplicants10].string
    requirement31 = json[SerializationKeys.requirement31].string
    grade9swdapplicants9 = json[SerializationKeys.grade9swdapplicants9].string
    requirement18 = json[SerializationKeys.requirement18].string
    grade9geapplicants9 = json[SerializationKeys.grade9geapplicants9].string
    website = json[SerializationKeys.website].string
    interest3 = json[SerializationKeys.interest3].string
    commonAudition6 = json[SerializationKeys.commonAudition6].string
    requirement54 = json[SerializationKeys.requirement54].string
    method8 = json[SerializationKeys.method8].string
    admissionspriority36 = json[SerializationKeys.admissionspriority36].string
    grade9gefilledflag9 = json[SerializationKeys.grade9gefilledflag9].string
    grade9geapplicantsperseat3 = json[SerializationKeys.grade9geapplicantsperseat3].string
    grade9gefilledflag10 = json[SerializationKeys.grade9gefilledflag10].string
    admissionspriority44 = json[SerializationKeys.admissionspriority44].string
    boro = json[SerializationKeys.boro].string
    totalStudents = json[SerializationKeys.totalStudents].string
    interest10 = json[SerializationKeys.interest10].string
    grade9geapplicants2 = json[SerializationKeys.grade9geapplicants2].string
    ellPrograms = json[SerializationKeys.ellPrograms].string
    auditioninformation2 = json[SerializationKeys.auditioninformation2].string
    interest7 = json[SerializationKeys.interest7].string
    requirement47 = json[SerializationKeys.requirement47].string
    admissionspriority63 = json[SerializationKeys.admissionspriority63].string
    grade9geapplicantsperseat4 = json[SerializationKeys.grade9geapplicantsperseat4].string
    admissionspriority32 = json[SerializationKeys.admissionspriority32].string
    requirement67 = json[SerializationKeys.requirement67].string
    seats9ge4 = json[SerializationKeys.seats9ge4].string
    finalgrades = json[SerializationKeys.finalgrades].string
    grade9gefilledflag3 = json[SerializationKeys.grade9gefilledflag3].string
    appperseat6specialized = json[SerializationKeys.appperseat6specialized].string
    interest6 = json[SerializationKeys.interest6].string
    seats9swd1 = json[SerializationKeys.seats9swd1].string
    grade9swdapplicantsperseat5 = json[SerializationKeys.grade9swdapplicantsperseat5].string
    offerRate1 = json[SerializationKeys.offerRate1].string
    extracurricularActivities = json[SerializationKeys.extracurricularActivities].string
    zip = json[SerializationKeys.zip].string
    faxNumber = json[SerializationKeys.faxNumber].string
    endTime = json[SerializationKeys.endTime].string
    requirement35 = json[SerializationKeys.requirement35].string
    admissionspriority13 = json[SerializationKeys.admissionspriority13].string
    admissionspriority37 = json[SerializationKeys.admissionspriority37].string
    auditioninformation7 = json[SerializationKeys.auditioninformation7].string
    longitude = json[SerializationKeys.longitude].string
    prgdesc4 = json[SerializationKeys.prgdesc4].string
    eligibility7 = json[SerializationKeys.eligibility7].string
    offerRate4 = json[SerializationKeys.offerRate4].string
    grade9geapplicants6 = json[SerializationKeys.grade9geapplicants6].string
    grade9swdapplicantsperseat8 = json[SerializationKeys.grade9swdapplicantsperseat8].string
    seats108 = json[SerializationKeys.seats108].string
    admissionspriority74 = json[SerializationKeys.admissionspriority74].string
    seats3specialized = json[SerializationKeys.seats3specialized].string
    requirement11 = json[SerializationKeys.requirement11].string
    grade9gefilledflag2 = json[SerializationKeys.grade9gefilledflag2].string
    seats101 = json[SerializationKeys.seats101].string
    interest8 = json[SerializationKeys.interest8].string
    commonAudition5 = json[SerializationKeys.commonAudition5].string
    appperseat5specialized = json[SerializationKeys.appperseat5specialized].string
    admissionspriority43 = json[SerializationKeys.admissionspriority43].string
    interest9 = json[SerializationKeys.interest9].string
    eligibility5 = json[SerializationKeys.eligibility5].string
    requirement34 = json[SerializationKeys.requirement34].string
    grade9swdapplicantsperseat10 = json[SerializationKeys.grade9swdapplicantsperseat10].string
    grade9swdfilledflag3 = json[SerializationKeys.grade9swdfilledflag3].string
    seats1010 = json[SerializationKeys.seats1010].string
    grade9swdfilledflag8 = json[SerializationKeys.grade9swdfilledflag8].string
    specialized = json[SerializationKeys.specialized].string
    grade9swdapplicants8 = json[SerializationKeys.grade9swdapplicants8].string
    appperseat2specialized = json[SerializationKeys.appperseat2specialized].string
    nta = json[SerializationKeys.nta].string
    academicopportunities3 = json[SerializationKeys.academicopportunities3].string
    geoeligibility = json[SerializationKeys.geoeligibility].string
    censusTract = json[SerializationKeys.censusTract].string
    requirement22 = json[SerializationKeys.requirement22].string
    pbat = json[SerializationKeys.pbat].string
    requirement38 = json[SerializationKeys.requirement38].string
    applicants1specialized = json[SerializationKeys.applicants1specialized].string
    eligibility4 = json[SerializationKeys.eligibility4].string
    prgdesc2 = json[SerializationKeys.prgdesc2].string
    girls = json[SerializationKeys.girls].string
    method5 = json[SerializationKeys.method5].string
    seats9swd5 = json[SerializationKeys.seats9swd5].string
    requirement26 = json[SerializationKeys.requirement26].string
    academicopportunities5 = json[SerializationKeys.academicopportunities5].string
    program8 = json[SerializationKeys.program8].string
    admissionspriority71 = json[SerializationKeys.admissionspriority71].string
    academicopportunities4 = json[SerializationKeys.academicopportunities4].string
    buildingCode = json[SerializationKeys.buildingCode].string
    councilDistrict = json[SerializationKeys.councilDistrict].string
    seats1specialized = json[SerializationKeys.seats1specialized].string
    directions5 = json[SerializationKeys.directions5].string
    appperseat4specialized = json[SerializationKeys.appperseat4specialized].string
    admissionspriority28 = json[SerializationKeys.admissionspriority28].string
    requirement61 = json[SerializationKeys.requirement61].string
    grade9gefilledflag8 = json[SerializationKeys.grade9gefilledflag8].string
    admissionspriority23 = json[SerializationKeys.admissionspriority23].string
    seats105 = json[SerializationKeys.seats105].string
    attendanceRate = json[SerializationKeys.attendanceRate].string
    admissionspriority21 = json[SerializationKeys.admissionspriority21].string
    psalSportsCoed = json[SerializationKeys.psalSportsCoed].string
    commonAudition3 = json[SerializationKeys.commonAudition3].string
    code10 = json[SerializationKeys.code10].string
    offerRate7 = json[SerializationKeys.offerRate7].string
    seats9swd9 = json[SerializationKeys.seats9swd9].string
    seats9ge2 = json[SerializationKeys.seats9ge2].string
    requirement56 = json[SerializationKeys.requirement56].string
    admissionspriority14 = json[SerializationKeys.admissionspriority14].string
    prgdesc6 = json[SerializationKeys.prgdesc6].string
    grade9swdapplicants1 = json[SerializationKeys.grade9swdapplicants1].string
    program5 = json[SerializationKeys.program5].string
    requirement45 = json[SerializationKeys.requirement45].string
    admissionspriority27 = json[SerializationKeys.admissionspriority27].string
    seats5specialized = json[SerializationKeys.seats5specialized].string
    advancedplacementCourses = json[SerializationKeys.advancedplacementCourses].string
    admissionspriority42 = json[SerializationKeys.admissionspriority42].string
    grade9swdfilledflag5 = json[SerializationKeys.grade9swdfilledflag5].string
    admissionspriority34 = json[SerializationKeys.admissionspriority34].string
    seats9ge1 = json[SerializationKeys.seats9ge1].string
    requirement41 = json[SerializationKeys.requirement41].string
    requirement14 = json[SerializationKeys.requirement14].string
    grade9swdapplicantsperseat4 = json[SerializationKeys.grade9swdapplicantsperseat4].string
    requirement52 = json[SerializationKeys.requirement52].string
    admissionspriority31 = json[SerializationKeys.admissionspriority31].string
    addtlInfo1 = json[SerializationKeys.addtlInfo1].string
    code3 = json[SerializationKeys.code3].string
    grade9swdapplicantsperseat7 = json[SerializationKeys.grade9swdapplicantsperseat7].string
    requirement63 = json[SerializationKeys.requirement63].string
    startTime = json[SerializationKeys.startTime].string
    eligibility6 = json[SerializationKeys.eligibility6].string
    seats9ge10 = json[SerializationKeys.seats9ge10].string
    requirement25 = json[SerializationKeys.requirement25].string
    requirement27 = json[SerializationKeys.requirement27].string
    school10thSeats = json[SerializationKeys.school10thSeats].string
    admissionspriority61 = json[SerializationKeys.admissionspriority61].string
    seats9swd2 = json[SerializationKeys.seats9swd2].string
    ptech = json[SerializationKeys.ptech].string
    prgdesc1 = json[SerializationKeys.prgdesc1].string
    admissionspriority29 = json[SerializationKeys.admissionspriority29].string
    grade9swdapplicantsperseat1 = json[SerializationKeys.grade9swdapplicantsperseat1].string
    seats9ge3 = json[SerializationKeys.seats9ge3].string
    admissionspriority11 = json[SerializationKeys.admissionspriority11].string
    campusName = json[SerializationKeys.campusName].string
    admissionspriority24 = json[SerializationKeys.admissionspriority24].string
    requirement43 = json[SerializationKeys.requirement43].string
    boys = json[SerializationKeys.boys].string
    method4 = json[SerializationKeys.method4].string
    requirement17 = json[SerializationKeys.requirement17].string
    requirement28 = json[SerializationKeys.requirement28].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = grade9swdapplicantsperseat6 { dictionary[SerializationKeys.grade9swdapplicantsperseat6] = value }
    if let value = requirement32 { dictionary[SerializationKeys.requirement32] = value }
    if let value = seats103 { dictionary[SerializationKeys.seats103] = value }
    if let value = grade9geapplicants7 { dictionary[SerializationKeys.grade9geapplicants7] = value }
    if let value = directions4 { dictionary[SerializationKeys.directions4] = value }
    if let value = interest2 { dictionary[SerializationKeys.interest2] = value }
    if let value = eligibility3 { dictionary[SerializationKeys.eligibility3] = value }
    if let value = code1 { dictionary[SerializationKeys.code1] = value }
    if let value = bbl { dictionary[SerializationKeys.bbl] = value }
    if let value = interest5 { dictionary[SerializationKeys.interest5] = value }
    if let value = admissionspriority25 { dictionary[SerializationKeys.admissionspriority25] = value }
    if let value = method7 { dictionary[SerializationKeys.method7] = value }
    if let value = seats106 { dictionary[SerializationKeys.seats106] = value }
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    if let value = grade9geapplicants8 { dictionary[SerializationKeys.grade9geapplicants8] = value }
    if let value = admissionspriority15 { dictionary[SerializationKeys.admissionspriority15] = value }
    if let value = prgdesc7 { dictionary[SerializationKeys.prgdesc7] = value }
    if let value = eligibility1 { dictionary[SerializationKeys.eligibility1] = value }
    if let value = grade9swdapplicants3 { dictionary[SerializationKeys.grade9swdapplicants3] = value }
    if let value = code8 { dictionary[SerializationKeys.code8] = value }
    if let value = commonAudition7 { dictionary[SerializationKeys.commonAudition7] = value }
    if let value = offerRate2 { dictionary[SerializationKeys.offerRate2] = value }
    if let value = grade9swdfilledflag6 { dictionary[SerializationKeys.grade9swdfilledflag6] = value }
    if let value = grade9gefilledflag1 { dictionary[SerializationKeys.grade9gefilledflag1] = value }
    if let value = admissionspriority17 { dictionary[SerializationKeys.admissionspriority17] = value }
    if let value = admissionspriority53 { dictionary[SerializationKeys.admissionspriority53] = value }
    if let value = admissionspriority41 { dictionary[SerializationKeys.admissionspriority41] = value }
    if let value = grade9gefilledflag6 { dictionary[SerializationKeys.grade9gefilledflag6] = value }
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = grade9geapplicantsperseat2 { dictionary[SerializationKeys.grade9geapplicantsperseat2] = value }
    if let value = seats9swd3 { dictionary[SerializationKeys.seats9swd3] = value }
    if let value = admissionspriority56 { dictionary[SerializationKeys.admissionspriority56] = value }
    if let value = eligibility2 { dictionary[SerializationKeys.eligibility2] = value }
    if let value = program7 { dictionary[SerializationKeys.program7] = value }
    if let value = seats9ge7 { dictionary[SerializationKeys.seats9ge7] = value }
    if let value = program4 { dictionary[SerializationKeys.program4] = value }
    if let value = commonAudition1 { dictionary[SerializationKeys.commonAudition1] = value }
    if let value = method9 { dictionary[SerializationKeys.method9] = value }
    if let value = auditioninformation4 { dictionary[SerializationKeys.auditioninformation4] = value }
    if let value = applicants4specialized { dictionary[SerializationKeys.applicants4specialized] = value }
    if let value = requirement15 { dictionary[SerializationKeys.requirement15] = value }
    if let value = grade9swdfilledflag10 { dictionary[SerializationKeys.grade9swdfilledflag10] = value }
    if let value = admissionspriority62 { dictionary[SerializationKeys.admissionspriority62] = value }
    if let value = seats9swd8 { dictionary[SerializationKeys.seats9swd8] = value }
    if let value = method3 { dictionary[SerializationKeys.method3] = value }
    if let value = grade9swdfilledflag2 { dictionary[SerializationKeys.grade9swdfilledflag2] = value }
    if let value = grade9swdapplicants5 { dictionary[SerializationKeys.grade9swdapplicants5] = value }
    if let value = offerRate6 { dictionary[SerializationKeys.offerRate6] = value }
    if let value = admissionspriority33 { dictionary[SerializationKeys.admissionspriority33] = value }
    if let value = primaryAddressLine1 { dictionary[SerializationKeys.primaryAddressLine1] = value }
    if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
    if let value = grade9geapplicantsperseat5 { dictionary[SerializationKeys.grade9geapplicantsperseat5] = value }
    if let value = seats4specialized { dictionary[SerializationKeys.seats4specialized] = value }
    if let value = overviewParagraph { dictionary[SerializationKeys.overviewParagraph] = value }
    if let value = program6 { dictionary[SerializationKeys.program6] = value }
    if let value = grade9gefilledflag5 { dictionary[SerializationKeys.grade9gefilledflag5] = value }
    if let value = grades2018 { dictionary[SerializationKeys.grades2018] = value }
    if let value = seats6specialized { dictionary[SerializationKeys.seats6specialized] = value }
    if let value = bin { dictionary[SerializationKeys.bin] = value }
    if let value = psalSportsBoys { dictionary[SerializationKeys.psalSportsBoys] = value }
    if let value = grade9gefilledflag4 { dictionary[SerializationKeys.grade9gefilledflag4] = value }
    if let value = communityBoard { dictionary[SerializationKeys.communityBoard] = value }
    if let value = grade9gefilledflag7 { dictionary[SerializationKeys.grade9gefilledflag7] = value }
    if let value = appperseat1specialized { dictionary[SerializationKeys.appperseat1specialized] = value }
    if let value = requirement51 { dictionary[SerializationKeys.requirement51] = value }
    if let value = admissionspriority64 { dictionary[SerializationKeys.admissionspriority64] = value }
    if let value = auditioninformation5 { dictionary[SerializationKeys.auditioninformation5] = value }
    if let value = grade9geapplicantsperseat8 { dictionary[SerializationKeys.grade9geapplicantsperseat8] = value }
    if let value = psalSportsGirls { dictionary[SerializationKeys.psalSportsGirls] = value }
    if let value = grade9geapplicants4 { dictionary[SerializationKeys.grade9geapplicants4] = value }
    if let value = grade9swdfilledflag1 { dictionary[SerializationKeys.grade9swdfilledflag1] = value }
    if let value = directions6 { dictionary[SerializationKeys.directions6] = value }
    if let value = admissionspriority35 { dictionary[SerializationKeys.admissionspriority35] = value }
    if let value = dbn { dictionary[SerializationKeys.dbn] = value }
    if let value = grade9geapplicantsperseat7 { dictionary[SerializationKeys.grade9geapplicantsperseat7] = value }
    if let value = grade9swdapplicants7 { dictionary[SerializationKeys.grade9swdapplicants7] = value }
    if let value = requirement21 { dictionary[SerializationKeys.requirement21] = value }
    if let value = code4 { dictionary[SerializationKeys.code4] = value }
    if let value = requirement44 { dictionary[SerializationKeys.requirement44] = value }
    if let value = seats109 { dictionary[SerializationKeys.seats109] = value }
    if let value = program9 { dictionary[SerializationKeys.program9] = value }
    if let value = seats104 { dictionary[SerializationKeys.seats104] = value }
    if let value = grade9swdfilledflag7 { dictionary[SerializationKeys.grade9swdfilledflag7] = value }
    if let value = directions1 { dictionary[SerializationKeys.directions1] = value }
    if let value = schoolEmail { dictionary[SerializationKeys.schoolEmail] = value }
    if let value = requirement62 { dictionary[SerializationKeys.requirement62] = value }
    if let value = requirement16 { dictionary[SerializationKeys.requirement16] = value }
    if let value = requirement23 { dictionary[SerializationKeys.requirement23] = value }
    if let value = code2 { dictionary[SerializationKeys.code2] = value }
    if let value = international { dictionary[SerializationKeys.international] = value }
    if let value = applicants5specialized { dictionary[SerializationKeys.applicants5specialized] = value }
    if let value = requirement42 { dictionary[SerializationKeys.requirement42] = value }
    if let value = appperseat3specialized { dictionary[SerializationKeys.appperseat3specialized] = value }
    if let value = schoolName { dictionary[SerializationKeys.schoolName] = value }
    if let value = collegeCareerRate { dictionary[SerializationKeys.collegeCareerRate] = value }
    if let value = applicants6specialized { dictionary[SerializationKeys.applicants6specialized] = value }
    if let value = seats9swd7 { dictionary[SerializationKeys.seats9swd7] = value }
    if let value = grade9geapplicantsperseat1 { dictionary[SerializationKeys.grade9geapplicantsperseat1] = value }
    if let value = borough { dictionary[SerializationKeys.borough] = value }
    if let value = admissionspriority54 { dictionary[SerializationKeys.admissionspriority54] = value }
    if let value = grade9swdapplicants10 { dictionary[SerializationKeys.grade9swdapplicants10] = value }
    if let value = grade9geapplicants1 { dictionary[SerializationKeys.grade9geapplicants1] = value }
    if let value = seats9ge6 { dictionary[SerializationKeys.seats9ge6] = value }
    if let value = interest4 { dictionary[SerializationKeys.interest4] = value }
    if let value = method10 { dictionary[SerializationKeys.method10] = value }
    if let value = requirement46 { dictionary[SerializationKeys.requirement46] = value }
    if let value = program1 { dictionary[SerializationKeys.program1] = value }
    if let value = admissionspriority52 { dictionary[SerializationKeys.admissionspriority52] = value }
    if let value = neighborhood { dictionary[SerializationKeys.neighborhood] = value }
    if let value = auditioninformation1 { dictionary[SerializationKeys.auditioninformation1] = value }
    if let value = grade9geapplicantsperseat6 { dictionary[SerializationKeys.grade9geapplicantsperseat6] = value }
    if let value = prgdesc3 { dictionary[SerializationKeys.prgdesc3] = value }
    if let value = seats2specialized { dictionary[SerializationKeys.seats2specialized] = value }
    if let value = requirement57 { dictionary[SerializationKeys.requirement57] = value }
    if let value = seats9swd10 { dictionary[SerializationKeys.seats9swd10] = value }
    if let value = grade9swdapplicants2 { dictionary[SerializationKeys.grade9swdapplicants2] = value }
    if let value = earlycollege { dictionary[SerializationKeys.earlycollege] = value }
    if let value = admissionspriority12 { dictionary[SerializationKeys.admissionspriority12] = value }
    if let value = admissionspriority19 { dictionary[SerializationKeys.admissionspriority19] = value }
    if let value = code5 { dictionary[SerializationKeys.code5] = value }
    if let value = directions2 { dictionary[SerializationKeys.directions2] = value }
    if let value = code7 { dictionary[SerializationKeys.code7] = value }
    if let value = seats9ge8 { dictionary[SerializationKeys.seats9ge8] = value }
    if let value = grade9swdapplicantsperseat9 { dictionary[SerializationKeys.grade9swdapplicantsperseat9] = value }
    if let value = schoolSports { dictionary[SerializationKeys.schoolSports] = value }
    if let value = requirement13 { dictionary[SerializationKeys.requirement13] = value }
    if let value = grade9geapplicantsperseat9 { dictionary[SerializationKeys.grade9geapplicantsperseat9] = value }
    if let value = diplomaendorsements { dictionary[SerializationKeys.diplomaendorsements] = value }
    if let value = interest1 { dictionary[SerializationKeys.interest1] = value }
    if let value = auditioninformation6 { dictionary[SerializationKeys.auditioninformation6] = value }
    if let value = schoolAccessibilityDescription { dictionary[SerializationKeys.schoolAccessibilityDescription] = value }
    if let value = grade9geapplicants3 { dictionary[SerializationKeys.grade9geapplicants3] = value }
    if let value = grade9swdfilledflag9 { dictionary[SerializationKeys.grade9swdfilledflag9] = value }
    if let value = program3 { dictionary[SerializationKeys.program3] = value }
    if let value = subway { dictionary[SerializationKeys.subway] = value }
    if let value = grade9swdapplicants4 { dictionary[SerializationKeys.grade9swdapplicants4] = value }
    if let value = program2 { dictionary[SerializationKeys.program2] = value }
    if let value = program10 { dictionary[SerializationKeys.program10] = value }
    if let value = offerRate3 { dictionary[SerializationKeys.offerRate3] = value }
    if let value = offerRate5 { dictionary[SerializationKeys.offerRate5] = value }
    if let value = applicants2specialized { dictionary[SerializationKeys.applicants2specialized] = value }
    if let value = admissionspriority16 { dictionary[SerializationKeys.admissionspriority16] = value }
    if let value = grade9geapplicantsperseat10 { dictionary[SerializationKeys.grade9geapplicantsperseat10] = value }
    if let value = offerRate8 { dictionary[SerializationKeys.offerRate8] = value }
    if let value = sharedSpace { dictionary[SerializationKeys.sharedSpace] = value }
    if let value = requirement55 { dictionary[SerializationKeys.requirement55] = value }
    if let value = method6 { dictionary[SerializationKeys.method6] = value }
    if let value = admissionspriority18 { dictionary[SerializationKeys.admissionspriority18] = value }
    if let value = seats9ge9 { dictionary[SerializationKeys.seats9ge9] = value }
    if let value = seats9swd6 { dictionary[SerializationKeys.seats9swd6] = value }
    if let value = auditioninformation3 { dictionary[SerializationKeys.auditioninformation3] = value }
    if let value = grade9swdapplicants6 { dictionary[SerializationKeys.grade9swdapplicants6] = value }
    if let value = admissionspriority26 { dictionary[SerializationKeys.admissionspriority26] = value }
    if let value = transfer { dictionary[SerializationKeys.transfer] = value }
    if let value = requirement33 { dictionary[SerializationKeys.requirement33] = value }
    if let value = directions3 { dictionary[SerializationKeys.directions3] = value }
    if let value = graduationRate { dictionary[SerializationKeys.graduationRate] = value }
    if let value = location { dictionary[SerializationKeys.location] = value }
    if let value = pctStuEnoughVariety { dictionary[SerializationKeys.pctStuEnoughVariety] = value }
    if let value = prgdesc9 { dictionary[SerializationKeys.prgdesc9] = value }
    if let value = prgdesc10 { dictionary[SerializationKeys.prgdesc10] = value }
    if let value = admissionspriority110 { dictionary[SerializationKeys.admissionspriority110] = value }
    if let value = languageClasses { dictionary[SerializationKeys.languageClasses] = value }
    if let value = commonAudition2 { dictionary[SerializationKeys.commonAudition2] = value }
    if let value = applicants3specialized { dictionary[SerializationKeys.applicants3specialized] = value }
    if let value = admissionspriority51 { dictionary[SerializationKeys.admissionspriority51] = value }
    if let value = seats9ge5 { dictionary[SerializationKeys.seats9ge5] = value }
    if let value = requirement37 { dictionary[SerializationKeys.requirement37] = value }
    if let value = requirement36 { dictionary[SerializationKeys.requirement36] = value }
    if let value = academicopportunities1 { dictionary[SerializationKeys.academicopportunities1] = value }
    if let value = pctStuSafe { dictionary[SerializationKeys.pctStuSafe] = value }
    if let value = bus { dictionary[SerializationKeys.bus] = value }
    if let value = requirement12 { dictionary[SerializationKeys.requirement12] = value }
    if let value = requirement53 { dictionary[SerializationKeys.requirement53] = value }
    if let value = stateCode { dictionary[SerializationKeys.stateCode] = value }
    if let value = grade9geapplicants5 { dictionary[SerializationKeys.grade9geapplicants5] = value }
    if let value = seats9swd4 { dictionary[SerializationKeys.seats9swd4] = value }
    if let value = code6 { dictionary[SerializationKeys.code6] = value }
    if let value = code9 { dictionary[SerializationKeys.code9] = value }
    if let value = grade9swdfilledflag4 { dictionary[SerializationKeys.grade9swdfilledflag4] = value }
    if let value = grade9swdapplicantsperseat3 { dictionary[SerializationKeys.grade9swdapplicantsperseat3] = value }
    if let value = prgdesc5 { dictionary[SerializationKeys.prgdesc5] = value }
    if let value = method2 { dictionary[SerializationKeys.method2] = value }
    if let value = admissionspriority22 { dictionary[SerializationKeys.admissionspriority22] = value }
    if let value = directions7 { dictionary[SerializationKeys.directions7] = value }
    if let value = admissionspriority46 { dictionary[SerializationKeys.admissionspriority46] = value }
    if let value = offerRate9 { dictionary[SerializationKeys.offerRate9] = value }
    if let value = seats107 { dictionary[SerializationKeys.seats107] = value }
    if let value = academicopportunities2 { dictionary[SerializationKeys.academicopportunities2] = value }
    if let value = grade9swdapplicantsperseat2 { dictionary[SerializationKeys.grade9swdapplicantsperseat2] = value }
    if let value = seats102 { dictionary[SerializationKeys.seats102] = value }
    if let value = requirement24 { dictionary[SerializationKeys.requirement24] = value }
    if let value = method1 { dictionary[SerializationKeys.method1] = value }
    if let value = commonAudition4 { dictionary[SerializationKeys.commonAudition4] = value }
    if let value = prgdesc8 { dictionary[SerializationKeys.prgdesc8] = value }
    if let value = grade9geapplicants10 { dictionary[SerializationKeys.grade9geapplicants10] = value }
    if let value = requirement31 { dictionary[SerializationKeys.requirement31] = value }
    if let value = grade9swdapplicants9 { dictionary[SerializationKeys.grade9swdapplicants9] = value }
    if let value = requirement18 { dictionary[SerializationKeys.requirement18] = value }
    if let value = grade9geapplicants9 { dictionary[SerializationKeys.grade9geapplicants9] = value }
    if let value = website { dictionary[SerializationKeys.website] = value }
    if let value = interest3 { dictionary[SerializationKeys.interest3] = value }
    if let value = commonAudition6 { dictionary[SerializationKeys.commonAudition6] = value }
    if let value = requirement54 { dictionary[SerializationKeys.requirement54] = value }
    if let value = method8 { dictionary[SerializationKeys.method8] = value }
    if let value = admissionspriority36 { dictionary[SerializationKeys.admissionspriority36] = value }
    if let value = grade9gefilledflag9 { dictionary[SerializationKeys.grade9gefilledflag9] = value }
    if let value = grade9geapplicantsperseat3 { dictionary[SerializationKeys.grade9geapplicantsperseat3] = value }
    if let value = grade9gefilledflag10 { dictionary[SerializationKeys.grade9gefilledflag10] = value }
    if let value = admissionspriority44 { dictionary[SerializationKeys.admissionspriority44] = value }
    if let value = boro { dictionary[SerializationKeys.boro] = value }
    if let value = totalStudents { dictionary[SerializationKeys.totalStudents] = value }
    if let value = interest10 { dictionary[SerializationKeys.interest10] = value }
    if let value = grade9geapplicants2 { dictionary[SerializationKeys.grade9geapplicants2] = value }
    if let value = ellPrograms { dictionary[SerializationKeys.ellPrograms] = value }
    if let value = auditioninformation2 { dictionary[SerializationKeys.auditioninformation2] = value }
    if let value = interest7 { dictionary[SerializationKeys.interest7] = value }
    if let value = requirement47 { dictionary[SerializationKeys.requirement47] = value }
    if let value = admissionspriority63 { dictionary[SerializationKeys.admissionspriority63] = value }
    if let value = grade9geapplicantsperseat4 { dictionary[SerializationKeys.grade9geapplicantsperseat4] = value }
    if let value = admissionspriority32 { dictionary[SerializationKeys.admissionspriority32] = value }
    if let value = requirement67 { dictionary[SerializationKeys.requirement67] = value }
    if let value = seats9ge4 { dictionary[SerializationKeys.seats9ge4] = value }
    if let value = finalgrades { dictionary[SerializationKeys.finalgrades] = value }
    if let value = grade9gefilledflag3 { dictionary[SerializationKeys.grade9gefilledflag3] = value }
    if let value = appperseat6specialized { dictionary[SerializationKeys.appperseat6specialized] = value }
    if let value = interest6 { dictionary[SerializationKeys.interest6] = value }
    if let value = seats9swd1 { dictionary[SerializationKeys.seats9swd1] = value }
    if let value = grade9swdapplicantsperseat5 { dictionary[SerializationKeys.grade9swdapplicantsperseat5] = value }
    if let value = offerRate1 { dictionary[SerializationKeys.offerRate1] = value }
    if let value = extracurricularActivities { dictionary[SerializationKeys.extracurricularActivities] = value }
    if let value = zip { dictionary[SerializationKeys.zip] = value }
    if let value = faxNumber { dictionary[SerializationKeys.faxNumber] = value }
    if let value = endTime { dictionary[SerializationKeys.endTime] = value }
    if let value = requirement35 { dictionary[SerializationKeys.requirement35] = value }
    if let value = admissionspriority13 { dictionary[SerializationKeys.admissionspriority13] = value }
    if let value = admissionspriority37 { dictionary[SerializationKeys.admissionspriority37] = value }
    if let value = auditioninformation7 { dictionary[SerializationKeys.auditioninformation7] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = prgdesc4 { dictionary[SerializationKeys.prgdesc4] = value }
    if let value = eligibility7 { dictionary[SerializationKeys.eligibility7] = value }
    if let value = offerRate4 { dictionary[SerializationKeys.offerRate4] = value }
    if let value = grade9geapplicants6 { dictionary[SerializationKeys.grade9geapplicants6] = value }
    if let value = grade9swdapplicantsperseat8 { dictionary[SerializationKeys.grade9swdapplicantsperseat8] = value }
    if let value = seats108 { dictionary[SerializationKeys.seats108] = value }
    if let value = admissionspriority74 { dictionary[SerializationKeys.admissionspriority74] = value }
    if let value = seats3specialized { dictionary[SerializationKeys.seats3specialized] = value }
    if let value = requirement11 { dictionary[SerializationKeys.requirement11] = value }
    if let value = grade9gefilledflag2 { dictionary[SerializationKeys.grade9gefilledflag2] = value }
    if let value = seats101 { dictionary[SerializationKeys.seats101] = value }
    if let value = interest8 { dictionary[SerializationKeys.interest8] = value }
    if let value = commonAudition5 { dictionary[SerializationKeys.commonAudition5] = value }
    if let value = appperseat5specialized { dictionary[SerializationKeys.appperseat5specialized] = value }
    if let value = admissionspriority43 { dictionary[SerializationKeys.admissionspriority43] = value }
    if let value = interest9 { dictionary[SerializationKeys.interest9] = value }
    if let value = eligibility5 { dictionary[SerializationKeys.eligibility5] = value }
    if let value = requirement34 { dictionary[SerializationKeys.requirement34] = value }
    if let value = grade9swdapplicantsperseat10 { dictionary[SerializationKeys.grade9swdapplicantsperseat10] = value }
    if let value = grade9swdfilledflag3 { dictionary[SerializationKeys.grade9swdfilledflag3] = value }
    if let value = seats1010 { dictionary[SerializationKeys.seats1010] = value }
    if let value = grade9swdfilledflag8 { dictionary[SerializationKeys.grade9swdfilledflag8] = value }
    if let value = specialized { dictionary[SerializationKeys.specialized] = value }
    if let value = grade9swdapplicants8 { dictionary[SerializationKeys.grade9swdapplicants8] = value }
    if let value = appperseat2specialized { dictionary[SerializationKeys.appperseat2specialized] = value }
    if let value = nta { dictionary[SerializationKeys.nta] = value }
    if let value = academicopportunities3 { dictionary[SerializationKeys.academicopportunities3] = value }
    if let value = geoeligibility { dictionary[SerializationKeys.geoeligibility] = value }
    if let value = censusTract { dictionary[SerializationKeys.censusTract] = value }
    if let value = requirement22 { dictionary[SerializationKeys.requirement22] = value }
    if let value = pbat { dictionary[SerializationKeys.pbat] = value }
    if let value = requirement38 { dictionary[SerializationKeys.requirement38] = value }
    if let value = applicants1specialized { dictionary[SerializationKeys.applicants1specialized] = value }
    if let value = eligibility4 { dictionary[SerializationKeys.eligibility4] = value }
    if let value = prgdesc2 { dictionary[SerializationKeys.prgdesc2] = value }
    if let value = girls { dictionary[SerializationKeys.girls] = value }
    if let value = method5 { dictionary[SerializationKeys.method5] = value }
    if let value = seats9swd5 { dictionary[SerializationKeys.seats9swd5] = value }
    if let value = requirement26 { dictionary[SerializationKeys.requirement26] = value }
    if let value = academicopportunities5 { dictionary[SerializationKeys.academicopportunities5] = value }
    if let value = program8 { dictionary[SerializationKeys.program8] = value }
    if let value = admissionspriority71 { dictionary[SerializationKeys.admissionspriority71] = value }
    if let value = academicopportunities4 { dictionary[SerializationKeys.academicopportunities4] = value }
    if let value = buildingCode { dictionary[SerializationKeys.buildingCode] = value }
    if let value = councilDistrict { dictionary[SerializationKeys.councilDistrict] = value }
    if let value = seats1specialized { dictionary[SerializationKeys.seats1specialized] = value }
    if let value = directions5 { dictionary[SerializationKeys.directions5] = value }
    if let value = appperseat4specialized { dictionary[SerializationKeys.appperseat4specialized] = value }
    if let value = admissionspriority28 { dictionary[SerializationKeys.admissionspriority28] = value }
    if let value = requirement61 { dictionary[SerializationKeys.requirement61] = value }
    if let value = grade9gefilledflag8 { dictionary[SerializationKeys.grade9gefilledflag8] = value }
    if let value = admissionspriority23 { dictionary[SerializationKeys.admissionspriority23] = value }
    if let value = seats105 { dictionary[SerializationKeys.seats105] = value }
    if let value = attendanceRate { dictionary[SerializationKeys.attendanceRate] = value }
    if let value = admissionspriority21 { dictionary[SerializationKeys.admissionspriority21] = value }
    if let value = psalSportsCoed { dictionary[SerializationKeys.psalSportsCoed] = value }
    if let value = commonAudition3 { dictionary[SerializationKeys.commonAudition3] = value }
    if let value = code10 { dictionary[SerializationKeys.code10] = value }
    if let value = offerRate7 { dictionary[SerializationKeys.offerRate7] = value }
    if let value = seats9swd9 { dictionary[SerializationKeys.seats9swd9] = value }
    if let value = seats9ge2 { dictionary[SerializationKeys.seats9ge2] = value }
    if let value = requirement56 { dictionary[SerializationKeys.requirement56] = value }
    if let value = admissionspriority14 { dictionary[SerializationKeys.admissionspriority14] = value }
    if let value = prgdesc6 { dictionary[SerializationKeys.prgdesc6] = value }
    if let value = grade9swdapplicants1 { dictionary[SerializationKeys.grade9swdapplicants1] = value }
    if let value = program5 { dictionary[SerializationKeys.program5] = value }
    if let value = requirement45 { dictionary[SerializationKeys.requirement45] = value }
    if let value = admissionspriority27 { dictionary[SerializationKeys.admissionspriority27] = value }
    if let value = seats5specialized { dictionary[SerializationKeys.seats5specialized] = value }
    if let value = advancedplacementCourses { dictionary[SerializationKeys.advancedplacementCourses] = value }
    if let value = admissionspriority42 { dictionary[SerializationKeys.admissionspriority42] = value }
    if let value = grade9swdfilledflag5 { dictionary[SerializationKeys.grade9swdfilledflag5] = value }
    if let value = admissionspriority34 { dictionary[SerializationKeys.admissionspriority34] = value }
    if let value = seats9ge1 { dictionary[SerializationKeys.seats9ge1] = value }
    if let value = requirement41 { dictionary[SerializationKeys.requirement41] = value }
    if let value = requirement14 { dictionary[SerializationKeys.requirement14] = value }
    if let value = grade9swdapplicantsperseat4 { dictionary[SerializationKeys.grade9swdapplicantsperseat4] = value }
    if let value = requirement52 { dictionary[SerializationKeys.requirement52] = value }
    if let value = admissionspriority31 { dictionary[SerializationKeys.admissionspriority31] = value }
    if let value = addtlInfo1 { dictionary[SerializationKeys.addtlInfo1] = value }
    if let value = code3 { dictionary[SerializationKeys.code3] = value }
    if let value = grade9swdapplicantsperseat7 { dictionary[SerializationKeys.grade9swdapplicantsperseat7] = value }
    if let value = requirement63 { dictionary[SerializationKeys.requirement63] = value }
    if let value = startTime { dictionary[SerializationKeys.startTime] = value }
    if let value = eligibility6 { dictionary[SerializationKeys.eligibility6] = value }
    if let value = seats9ge10 { dictionary[SerializationKeys.seats9ge10] = value }
    if let value = requirement25 { dictionary[SerializationKeys.requirement25] = value }
    if let value = requirement27 { dictionary[SerializationKeys.requirement27] = value }
    if let value = school10thSeats { dictionary[SerializationKeys.school10thSeats] = value }
    if let value = admissionspriority61 { dictionary[SerializationKeys.admissionspriority61] = value }
    if let value = seats9swd2 { dictionary[SerializationKeys.seats9swd2] = value }
    if let value = ptech { dictionary[SerializationKeys.ptech] = value }
    if let value = prgdesc1 { dictionary[SerializationKeys.prgdesc1] = value }
    if let value = admissionspriority29 { dictionary[SerializationKeys.admissionspriority29] = value }
    if let value = grade9swdapplicantsperseat1 { dictionary[SerializationKeys.grade9swdapplicantsperseat1] = value }
    if let value = seats9ge3 { dictionary[SerializationKeys.seats9ge3] = value }
    if let value = admissionspriority11 { dictionary[SerializationKeys.admissionspriority11] = value }
    if let value = campusName { dictionary[SerializationKeys.campusName] = value }
    if let value = admissionspriority24 { dictionary[SerializationKeys.admissionspriority24] = value }
    if let value = requirement43 { dictionary[SerializationKeys.requirement43] = value }
    if let value = boys { dictionary[SerializationKeys.boys] = value }
    if let value = method4 { dictionary[SerializationKeys.method4] = value }
    if let value = requirement17 { dictionary[SerializationKeys.requirement17] = value }
    if let value = requirement28 { dictionary[SerializationKeys.requirement28] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.grade9swdapplicantsperseat6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat6) as? String
    self.requirement32 = aDecoder.decodeObject(forKey: SerializationKeys.requirement32) as? String
    self.seats103 = aDecoder.decodeObject(forKey: SerializationKeys.seats103) as? String
    self.grade9geapplicants7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants7) as? String
    self.directions4 = aDecoder.decodeObject(forKey: SerializationKeys.directions4) as? String
    self.interest2 = aDecoder.decodeObject(forKey: SerializationKeys.interest2) as? String
    self.eligibility3 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility3) as? String
    self.code1 = aDecoder.decodeObject(forKey: SerializationKeys.code1) as? String
    self.bbl = aDecoder.decodeObject(forKey: SerializationKeys.bbl) as? String
    self.interest5 = aDecoder.decodeObject(forKey: SerializationKeys.interest5) as? String
    self.admissionspriority25 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority25) as? String
    self.method7 = aDecoder.decodeObject(forKey: SerializationKeys.method7) as? String
    self.seats106 = aDecoder.decodeObject(forKey: SerializationKeys.seats106) as? String
    self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
    self.grade9geapplicants8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants8) as? String
    self.admissionspriority15 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority15) as? String
    self.prgdesc7 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc7) as? String
    self.eligibility1 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility1) as? String
    self.grade9swdapplicants3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants3) as? String
    self.code8 = aDecoder.decodeObject(forKey: SerializationKeys.code8) as? String
    self.commonAudition7 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition7) as? String
    self.offerRate2 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate2) as? String
    self.grade9swdfilledflag6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag6) as? String
    self.grade9gefilledflag1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag1) as? String
    self.admissionspriority17 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority17) as? String
    self.admissionspriority53 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority53) as? String
    self.admissionspriority41 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority41) as? String
    self.grade9gefilledflag6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag6) as? String
    self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
    self.grade9geapplicantsperseat2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat2) as? String
    self.seats9swd3 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd3) as? String
    self.admissionspriority56 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority56) as? String
    self.eligibility2 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility2) as? String
    self.program7 = aDecoder.decodeObject(forKey: SerializationKeys.program7) as? String
    self.seats9ge7 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge7) as? String
    self.program4 = aDecoder.decodeObject(forKey: SerializationKeys.program4) as? String
    self.commonAudition1 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition1) as? String
    self.method9 = aDecoder.decodeObject(forKey: SerializationKeys.method9) as? String
    self.auditioninformation4 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation4) as? String
    self.applicants4specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants4specialized) as? String
    self.requirement15 = aDecoder.decodeObject(forKey: SerializationKeys.requirement15) as? String
    self.grade9swdfilledflag10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag10) as? String
    self.admissionspriority62 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority62) as? String
    self.seats9swd8 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd8) as? String
    self.method3 = aDecoder.decodeObject(forKey: SerializationKeys.method3) as? String
    self.grade9swdfilledflag2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag2) as? String
    self.grade9swdapplicants5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants5) as? String
    self.offerRate6 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate6) as? String
    self.admissionspriority33 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority33) as? String
    self.primaryAddressLine1 = aDecoder.decodeObject(forKey: SerializationKeys.primaryAddressLine1) as? String
    self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
    self.grade9geapplicantsperseat5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat5) as? String
    self.seats4specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats4specialized) as? String
    self.overviewParagraph = aDecoder.decodeObject(forKey: SerializationKeys.overviewParagraph) as? String
    self.program6 = aDecoder.decodeObject(forKey: SerializationKeys.program6) as? String
    self.grade9gefilledflag5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag5) as? String
    self.grades2018 = aDecoder.decodeObject(forKey: SerializationKeys.grades2018) as? String
    self.seats6specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats6specialized) as? String
    self.bin = aDecoder.decodeObject(forKey: SerializationKeys.bin) as? String
    self.psalSportsBoys = aDecoder.decodeObject(forKey: SerializationKeys.psalSportsBoys) as? String
    self.grade9gefilledflag4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag4) as? String
    self.communityBoard = aDecoder.decodeObject(forKey: SerializationKeys.communityBoard) as? String
    self.grade9gefilledflag7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag7) as? String
    self.appperseat1specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat1specialized) as? String
    self.requirement51 = aDecoder.decodeObject(forKey: SerializationKeys.requirement51) as? String
    self.admissionspriority64 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority64) as? String
    self.auditioninformation5 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation5) as? String
    self.grade9geapplicantsperseat8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat8) as? String
    self.psalSportsGirls = aDecoder.decodeObject(forKey: SerializationKeys.psalSportsGirls) as? String
    self.grade9geapplicants4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants4) as? String
    self.grade9swdfilledflag1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag1) as? String
    self.directions6 = aDecoder.decodeObject(forKey: SerializationKeys.directions6) as? String
    self.admissionspriority35 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority35) as? String
    self.dbn = aDecoder.decodeObject(forKey: SerializationKeys.dbn) as? String
    self.grade9geapplicantsperseat7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat7) as? String
    self.grade9swdapplicants7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants7) as? String
    self.requirement21 = aDecoder.decodeObject(forKey: SerializationKeys.requirement21) as? String
    self.code4 = aDecoder.decodeObject(forKey: SerializationKeys.code4) as? String
    self.requirement44 = aDecoder.decodeObject(forKey: SerializationKeys.requirement44) as? String
    self.seats109 = aDecoder.decodeObject(forKey: SerializationKeys.seats109) as? String
    self.program9 = aDecoder.decodeObject(forKey: SerializationKeys.program9) as? String
    self.seats104 = aDecoder.decodeObject(forKey: SerializationKeys.seats104) as? String
    self.grade9swdfilledflag7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag7) as? String
    self.directions1 = aDecoder.decodeObject(forKey: SerializationKeys.directions1) as? String
    self.schoolEmail = aDecoder.decodeObject(forKey: SerializationKeys.schoolEmail) as? String
    self.requirement62 = aDecoder.decodeObject(forKey: SerializationKeys.requirement62) as? String
    self.requirement16 = aDecoder.decodeObject(forKey: SerializationKeys.requirement16) as? String
    self.requirement23 = aDecoder.decodeObject(forKey: SerializationKeys.requirement23) as? String
    self.code2 = aDecoder.decodeObject(forKey: SerializationKeys.code2) as? String
    self.international = aDecoder.decodeObject(forKey: SerializationKeys.international) as? String
    self.applicants5specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants5specialized) as? String
    self.requirement42 = aDecoder.decodeObject(forKey: SerializationKeys.requirement42) as? String
    self.appperseat3specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat3specialized) as? String
    self.schoolName = aDecoder.decodeObject(forKey: SerializationKeys.schoolName) as? String
    self.collegeCareerRate = aDecoder.decodeObject(forKey: SerializationKeys.collegeCareerRate) as? String
    self.applicants6specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants6specialized) as? String
    self.seats9swd7 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd7) as? String
    self.grade9geapplicantsperseat1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat1) as? String
    self.borough = aDecoder.decodeObject(forKey: SerializationKeys.borough) as? String
    self.admissionspriority54 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority54) as? String
    self.grade9swdapplicants10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants10) as? String
    self.grade9geapplicants1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants1) as? String
    self.seats9ge6 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge6) as? String
    self.interest4 = aDecoder.decodeObject(forKey: SerializationKeys.interest4) as? String
    self.method10 = aDecoder.decodeObject(forKey: SerializationKeys.method10) as? String
    self.requirement46 = aDecoder.decodeObject(forKey: SerializationKeys.requirement46) as? String
    self.program1 = aDecoder.decodeObject(forKey: SerializationKeys.program1) as? String
    self.admissionspriority52 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority52) as? String
    self.neighborhood = aDecoder.decodeObject(forKey: SerializationKeys.neighborhood) as? String
    self.auditioninformation1 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation1) as? String
    self.grade9geapplicantsperseat6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat6) as? String
    self.prgdesc3 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc3) as? String
    self.seats2specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats2specialized) as? String
    self.requirement57 = aDecoder.decodeObject(forKey: SerializationKeys.requirement57) as? String
    self.seats9swd10 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd10) as? String
    self.grade9swdapplicants2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants2) as? String
    self.earlycollege = aDecoder.decodeObject(forKey: SerializationKeys.earlycollege) as? String
    self.admissionspriority12 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority12) as? String
    self.admissionspriority19 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority19) as? String
    self.code5 = aDecoder.decodeObject(forKey: SerializationKeys.code5) as? String
    self.directions2 = aDecoder.decodeObject(forKey: SerializationKeys.directions2) as? String
    self.code7 = aDecoder.decodeObject(forKey: SerializationKeys.code7) as? String
    self.seats9ge8 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge8) as? String
    self.grade9swdapplicantsperseat9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat9) as? String
    self.schoolSports = aDecoder.decodeObject(forKey: SerializationKeys.schoolSports) as? String
    self.requirement13 = aDecoder.decodeObject(forKey: SerializationKeys.requirement13) as? String
    self.grade9geapplicantsperseat9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat9) as? String
    self.diplomaendorsements = aDecoder.decodeObject(forKey: SerializationKeys.diplomaendorsements) as? String
    self.interest1 = aDecoder.decodeObject(forKey: SerializationKeys.interest1) as? String
    self.auditioninformation6 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation6) as? String
    self.schoolAccessibilityDescription = aDecoder.decodeObject(forKey: SerializationKeys.schoolAccessibilityDescription) as? String
    self.grade9geapplicants3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants3) as? String
    self.grade9swdfilledflag9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag9) as? String
    self.program3 = aDecoder.decodeObject(forKey: SerializationKeys.program3) as? String
    self.subway = aDecoder.decodeObject(forKey: SerializationKeys.subway) as? String
    self.grade9swdapplicants4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants4) as? String
    self.program2 = aDecoder.decodeObject(forKey: SerializationKeys.program2) as? String
    self.program10 = aDecoder.decodeObject(forKey: SerializationKeys.program10) as? String
    self.offerRate3 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate3) as? String
    self.offerRate5 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate5) as? String
    self.applicants2specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants2specialized) as? String
    self.admissionspriority16 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority16) as? String
    self.grade9geapplicantsperseat10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat10) as? String
    self.offerRate8 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate8) as? String
    self.sharedSpace = aDecoder.decodeObject(forKey: SerializationKeys.sharedSpace) as? String
    self.requirement55 = aDecoder.decodeObject(forKey: SerializationKeys.requirement55) as? String
    self.method6 = aDecoder.decodeObject(forKey: SerializationKeys.method6) as? String
    self.admissionspriority18 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority18) as? String
    self.seats9ge9 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge9) as? String
    self.seats9swd6 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd6) as? String
    self.auditioninformation3 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation3) as? String
    self.grade9swdapplicants6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants6) as? String
    self.admissionspriority26 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority26) as? String
    self.transfer = aDecoder.decodeObject(forKey: SerializationKeys.transfer) as? String
    self.requirement33 = aDecoder.decodeObject(forKey: SerializationKeys.requirement33) as? String
    self.directions3 = aDecoder.decodeObject(forKey: SerializationKeys.directions3) as? String
    self.graduationRate = aDecoder.decodeObject(forKey: SerializationKeys.graduationRate) as? String
    self.location = aDecoder.decodeObject(forKey: SerializationKeys.location) as? String
    self.pctStuEnoughVariety = aDecoder.decodeObject(forKey: SerializationKeys.pctStuEnoughVariety) as? String
    self.prgdesc9 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc9) as? String
    self.prgdesc10 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc10) as? String
    self.admissionspriority110 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority110) as? String
    self.languageClasses = aDecoder.decodeObject(forKey: SerializationKeys.languageClasses) as? String
    self.commonAudition2 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition2) as? String
    self.applicants3specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants3specialized) as? String
    self.admissionspriority51 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority51) as? String
    self.seats9ge5 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge5) as? String
    self.requirement37 = aDecoder.decodeObject(forKey: SerializationKeys.requirement37) as? String
    self.requirement36 = aDecoder.decodeObject(forKey: SerializationKeys.requirement36) as? String
    self.academicopportunities1 = aDecoder.decodeObject(forKey: SerializationKeys.academicopportunities1) as? String
    self.pctStuSafe = aDecoder.decodeObject(forKey: SerializationKeys.pctStuSafe) as? String
    self.bus = aDecoder.decodeObject(forKey: SerializationKeys.bus) as? String
    self.requirement12 = aDecoder.decodeObject(forKey: SerializationKeys.requirement12) as? String
    self.requirement53 = aDecoder.decodeObject(forKey: SerializationKeys.requirement53) as? String
    self.stateCode = aDecoder.decodeObject(forKey: SerializationKeys.stateCode) as? String
    self.grade9geapplicants5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants5) as? String
    self.seats9swd4 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd4) as? String
    self.code6 = aDecoder.decodeObject(forKey: SerializationKeys.code6) as? String
    self.code9 = aDecoder.decodeObject(forKey: SerializationKeys.code9) as? String
    self.grade9swdfilledflag4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag4) as? String
    self.grade9swdapplicantsperseat3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat3) as? String
    self.prgdesc5 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc5) as? String
    self.method2 = aDecoder.decodeObject(forKey: SerializationKeys.method2) as? String
    self.admissionspriority22 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority22) as? String
    self.directions7 = aDecoder.decodeObject(forKey: SerializationKeys.directions7) as? String
    self.admissionspriority46 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority46) as? String
    self.offerRate9 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate9) as? String
    self.seats107 = aDecoder.decodeObject(forKey: SerializationKeys.seats107) as? String
    self.academicopportunities2 = aDecoder.decodeObject(forKey: SerializationKeys.academicopportunities2) as? String
    self.grade9swdapplicantsperseat2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat2) as? String
    self.seats102 = aDecoder.decodeObject(forKey: SerializationKeys.seats102) as? String
    self.requirement24 = aDecoder.decodeObject(forKey: SerializationKeys.requirement24) as? String
    self.method1 = aDecoder.decodeObject(forKey: SerializationKeys.method1) as? String
    self.commonAudition4 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition4) as? String
    self.prgdesc8 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc8) as? String
    self.grade9geapplicants10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants10) as? String
    self.requirement31 = aDecoder.decodeObject(forKey: SerializationKeys.requirement31) as? String
    self.grade9swdapplicants9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants9) as? String
    self.requirement18 = aDecoder.decodeObject(forKey: SerializationKeys.requirement18) as? String
    self.grade9geapplicants9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants9) as? String
    self.website = aDecoder.decodeObject(forKey: SerializationKeys.website) as? String
    self.interest3 = aDecoder.decodeObject(forKey: SerializationKeys.interest3) as? String
    self.commonAudition6 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition6) as? String
    self.requirement54 = aDecoder.decodeObject(forKey: SerializationKeys.requirement54) as? String
    self.method8 = aDecoder.decodeObject(forKey: SerializationKeys.method8) as? String
    self.admissionspriority36 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority36) as? String
    self.grade9gefilledflag9 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag9) as? String
    self.grade9geapplicantsperseat3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat3) as? String
    self.grade9gefilledflag10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag10) as? String
    self.admissionspriority44 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority44) as? String
    self.boro = aDecoder.decodeObject(forKey: SerializationKeys.boro) as? String
    self.totalStudents = aDecoder.decodeObject(forKey: SerializationKeys.totalStudents) as? String
    self.interest10 = aDecoder.decodeObject(forKey: SerializationKeys.interest10) as? String
    self.grade9geapplicants2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants2) as? String
    self.ellPrograms = aDecoder.decodeObject(forKey: SerializationKeys.ellPrograms) as? String
    self.auditioninformation2 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation2) as? String
    self.interest7 = aDecoder.decodeObject(forKey: SerializationKeys.interest7) as? String
    self.requirement47 = aDecoder.decodeObject(forKey: SerializationKeys.requirement47) as? String
    self.admissionspriority63 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority63) as? String
    self.grade9geapplicantsperseat4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicantsperseat4) as? String
    self.admissionspriority32 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority32) as? String
    self.requirement67 = aDecoder.decodeObject(forKey: SerializationKeys.requirement67) as? String
    self.seats9ge4 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge4) as? String
    self.finalgrades = aDecoder.decodeObject(forKey: SerializationKeys.finalgrades) as? String
    self.grade9gefilledflag3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag3) as? String
    self.appperseat6specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat6specialized) as? String
    self.interest6 = aDecoder.decodeObject(forKey: SerializationKeys.interest6) as? String
    self.seats9swd1 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd1) as? String
    self.grade9swdapplicantsperseat5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat5) as? String
    self.offerRate1 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate1) as? String
    self.extracurricularActivities = aDecoder.decodeObject(forKey: SerializationKeys.extracurricularActivities) as? String
    self.zip = aDecoder.decodeObject(forKey: SerializationKeys.zip) as? String
    self.faxNumber = aDecoder.decodeObject(forKey: SerializationKeys.faxNumber) as? String
    self.endTime = aDecoder.decodeObject(forKey: SerializationKeys.endTime) as? String
    self.requirement35 = aDecoder.decodeObject(forKey: SerializationKeys.requirement35) as? String
    self.admissionspriority13 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority13) as? String
    self.admissionspriority37 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority37) as? String
    self.auditioninformation7 = aDecoder.decodeObject(forKey: SerializationKeys.auditioninformation7) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    self.prgdesc4 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc4) as? String
    self.eligibility7 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility7) as? String
    self.offerRate4 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate4) as? String
    self.grade9geapplicants6 = aDecoder.decodeObject(forKey: SerializationKeys.grade9geapplicants6) as? String
    self.grade9swdapplicantsperseat8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat8) as? String
    self.seats108 = aDecoder.decodeObject(forKey: SerializationKeys.seats108) as? String
    self.admissionspriority74 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority74) as? String
    self.seats3specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats3specialized) as? String
    self.requirement11 = aDecoder.decodeObject(forKey: SerializationKeys.requirement11) as? String
    self.grade9gefilledflag2 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag2) as? String
    self.seats101 = aDecoder.decodeObject(forKey: SerializationKeys.seats101) as? String
    self.interest8 = aDecoder.decodeObject(forKey: SerializationKeys.interest8) as? String
    self.commonAudition5 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition5) as? String
    self.appperseat5specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat5specialized) as? String
    self.admissionspriority43 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority43) as? String
    self.interest9 = aDecoder.decodeObject(forKey: SerializationKeys.interest9) as? String
    self.eligibility5 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility5) as? String
    self.requirement34 = aDecoder.decodeObject(forKey: SerializationKeys.requirement34) as? String
    self.grade9swdapplicantsperseat10 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat10) as? String
    self.grade9swdfilledflag3 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag3) as? String
    self.seats1010 = aDecoder.decodeObject(forKey: SerializationKeys.seats1010) as? String
    self.grade9swdfilledflag8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag8) as? String
    self.specialized = aDecoder.decodeObject(forKey: SerializationKeys.specialized) as? String
    self.grade9swdapplicants8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants8) as? String
    self.appperseat2specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat2specialized) as? String
    self.nta = aDecoder.decodeObject(forKey: SerializationKeys.nta) as? String
    self.academicopportunities3 = aDecoder.decodeObject(forKey: SerializationKeys.academicopportunities3) as? String
    self.geoeligibility = aDecoder.decodeObject(forKey: SerializationKeys.geoeligibility) as? String
    self.censusTract = aDecoder.decodeObject(forKey: SerializationKeys.censusTract) as? String
    self.requirement22 = aDecoder.decodeObject(forKey: SerializationKeys.requirement22) as? String
    self.pbat = aDecoder.decodeObject(forKey: SerializationKeys.pbat) as? String
    self.requirement38 = aDecoder.decodeObject(forKey: SerializationKeys.requirement38) as? String
    self.applicants1specialized = aDecoder.decodeObject(forKey: SerializationKeys.applicants1specialized) as? String
    self.eligibility4 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility4) as? String
    self.prgdesc2 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc2) as? String
    self.girls = aDecoder.decodeObject(forKey: SerializationKeys.girls) as? String
    self.method5 = aDecoder.decodeObject(forKey: SerializationKeys.method5) as? String
    self.seats9swd5 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd5) as? String
    self.requirement26 = aDecoder.decodeObject(forKey: SerializationKeys.requirement26) as? String
    self.academicopportunities5 = aDecoder.decodeObject(forKey: SerializationKeys.academicopportunities5) as? String
    self.program8 = aDecoder.decodeObject(forKey: SerializationKeys.program8) as? String
    self.admissionspriority71 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority71) as? String
    self.academicopportunities4 = aDecoder.decodeObject(forKey: SerializationKeys.academicopportunities4) as? String
    self.buildingCode = aDecoder.decodeObject(forKey: SerializationKeys.buildingCode) as? String
    self.councilDistrict = aDecoder.decodeObject(forKey: SerializationKeys.councilDistrict) as? String
    self.seats1specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats1specialized) as? String
    self.directions5 = aDecoder.decodeObject(forKey: SerializationKeys.directions5) as? String
    self.appperseat4specialized = aDecoder.decodeObject(forKey: SerializationKeys.appperseat4specialized) as? String
    self.admissionspriority28 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority28) as? String
    self.requirement61 = aDecoder.decodeObject(forKey: SerializationKeys.requirement61) as? String
    self.grade9gefilledflag8 = aDecoder.decodeObject(forKey: SerializationKeys.grade9gefilledflag8) as? String
    self.admissionspriority23 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority23) as? String
    self.seats105 = aDecoder.decodeObject(forKey: SerializationKeys.seats105) as? String
    self.attendanceRate = aDecoder.decodeObject(forKey: SerializationKeys.attendanceRate) as? String
    self.admissionspriority21 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority21) as? String
    self.psalSportsCoed = aDecoder.decodeObject(forKey: SerializationKeys.psalSportsCoed) as? String
    self.commonAudition3 = aDecoder.decodeObject(forKey: SerializationKeys.commonAudition3) as? String
    self.code10 = aDecoder.decodeObject(forKey: SerializationKeys.code10) as? String
    self.offerRate7 = aDecoder.decodeObject(forKey: SerializationKeys.offerRate7) as? String
    self.seats9swd9 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd9) as? String
    self.seats9ge2 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge2) as? String
    self.requirement56 = aDecoder.decodeObject(forKey: SerializationKeys.requirement56) as? String
    self.admissionspriority14 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority14) as? String
    self.prgdesc6 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc6) as? String
    self.grade9swdapplicants1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicants1) as? String
    self.program5 = aDecoder.decodeObject(forKey: SerializationKeys.program5) as? String
    self.requirement45 = aDecoder.decodeObject(forKey: SerializationKeys.requirement45) as? String
    self.admissionspriority27 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority27) as? String
    self.seats5specialized = aDecoder.decodeObject(forKey: SerializationKeys.seats5specialized) as? String
    self.advancedplacementCourses = aDecoder.decodeObject(forKey: SerializationKeys.advancedplacementCourses) as? String
    self.admissionspriority42 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority42) as? String
    self.grade9swdfilledflag5 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdfilledflag5) as? String
    self.admissionspriority34 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority34) as? String
    self.seats9ge1 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge1) as? String
    self.requirement41 = aDecoder.decodeObject(forKey: SerializationKeys.requirement41) as? String
    self.requirement14 = aDecoder.decodeObject(forKey: SerializationKeys.requirement14) as? String
    self.grade9swdapplicantsperseat4 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat4) as? String
    self.requirement52 = aDecoder.decodeObject(forKey: SerializationKeys.requirement52) as? String
    self.admissionspriority31 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority31) as? String
    self.addtlInfo1 = aDecoder.decodeObject(forKey: SerializationKeys.addtlInfo1) as? String
    self.code3 = aDecoder.decodeObject(forKey: SerializationKeys.code3) as? String
    self.grade9swdapplicantsperseat7 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat7) as? String
    self.requirement63 = aDecoder.decodeObject(forKey: SerializationKeys.requirement63) as? String
    self.startTime = aDecoder.decodeObject(forKey: SerializationKeys.startTime) as? String
    self.eligibility6 = aDecoder.decodeObject(forKey: SerializationKeys.eligibility6) as? String
    self.seats9ge10 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge10) as? String
    self.requirement25 = aDecoder.decodeObject(forKey: SerializationKeys.requirement25) as? String
    self.requirement27 = aDecoder.decodeObject(forKey: SerializationKeys.requirement27) as? String
    self.school10thSeats = aDecoder.decodeObject(forKey: SerializationKeys.school10thSeats) as? String
    self.admissionspriority61 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority61) as? String
    self.seats9swd2 = aDecoder.decodeObject(forKey: SerializationKeys.seats9swd2) as? String
    self.ptech = aDecoder.decodeObject(forKey: SerializationKeys.ptech) as? String
    self.prgdesc1 = aDecoder.decodeObject(forKey: SerializationKeys.prgdesc1) as? String
    self.admissionspriority29 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority29) as? String
    self.grade9swdapplicantsperseat1 = aDecoder.decodeObject(forKey: SerializationKeys.grade9swdapplicantsperseat1) as? String
    self.seats9ge3 = aDecoder.decodeObject(forKey: SerializationKeys.seats9ge3) as? String
    self.admissionspriority11 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority11) as? String
    self.campusName = aDecoder.decodeObject(forKey: SerializationKeys.campusName) as? String
    self.admissionspriority24 = aDecoder.decodeObject(forKey: SerializationKeys.admissionspriority24) as? String
    self.requirement43 = aDecoder.decodeObject(forKey: SerializationKeys.requirement43) as? String
    self.boys = aDecoder.decodeObject(forKey: SerializationKeys.boys) as? String
    self.method4 = aDecoder.decodeObject(forKey: SerializationKeys.method4) as? String
    self.requirement17 = aDecoder.decodeObject(forKey: SerializationKeys.requirement17) as? String
    self.requirement28 = aDecoder.decodeObject(forKey: SerializationKeys.requirement28) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(grade9swdapplicantsperseat6, forKey: SerializationKeys.grade9swdapplicantsperseat6)
    aCoder.encode(requirement32, forKey: SerializationKeys.requirement32)
    aCoder.encode(seats103, forKey: SerializationKeys.seats103)
    aCoder.encode(grade9geapplicants7, forKey: SerializationKeys.grade9geapplicants7)
    aCoder.encode(directions4, forKey: SerializationKeys.directions4)
    aCoder.encode(interest2, forKey: SerializationKeys.interest2)
    aCoder.encode(eligibility3, forKey: SerializationKeys.eligibility3)
    aCoder.encode(code1, forKey: SerializationKeys.code1)
    aCoder.encode(bbl, forKey: SerializationKeys.bbl)
    aCoder.encode(interest5, forKey: SerializationKeys.interest5)
    aCoder.encode(admissionspriority25, forKey: SerializationKeys.admissionspriority25)
    aCoder.encode(method7, forKey: SerializationKeys.method7)
    aCoder.encode(seats106, forKey: SerializationKeys.seats106)
    aCoder.encode(latitude, forKey: SerializationKeys.latitude)
    aCoder.encode(grade9geapplicants8, forKey: SerializationKeys.grade9geapplicants8)
    aCoder.encode(admissionspriority15, forKey: SerializationKeys.admissionspriority15)
    aCoder.encode(prgdesc7, forKey: SerializationKeys.prgdesc7)
    aCoder.encode(eligibility1, forKey: SerializationKeys.eligibility1)
    aCoder.encode(grade9swdapplicants3, forKey: SerializationKeys.grade9swdapplicants3)
    aCoder.encode(code8, forKey: SerializationKeys.code8)
    aCoder.encode(commonAudition7, forKey: SerializationKeys.commonAudition7)
    aCoder.encode(offerRate2, forKey: SerializationKeys.offerRate2)
    aCoder.encode(grade9swdfilledflag6, forKey: SerializationKeys.grade9swdfilledflag6)
    aCoder.encode(grade9gefilledflag1, forKey: SerializationKeys.grade9gefilledflag1)
    aCoder.encode(admissionspriority17, forKey: SerializationKeys.admissionspriority17)
    aCoder.encode(admissionspriority53, forKey: SerializationKeys.admissionspriority53)
    aCoder.encode(admissionspriority41, forKey: SerializationKeys.admissionspriority41)
    aCoder.encode(grade9gefilledflag6, forKey: SerializationKeys.grade9gefilledflag6)
    aCoder.encode(city, forKey: SerializationKeys.city)
    aCoder.encode(grade9geapplicantsperseat2, forKey: SerializationKeys.grade9geapplicantsperseat2)
    aCoder.encode(seats9swd3, forKey: SerializationKeys.seats9swd3)
    aCoder.encode(admissionspriority56, forKey: SerializationKeys.admissionspriority56)
    aCoder.encode(eligibility2, forKey: SerializationKeys.eligibility2)
    aCoder.encode(program7, forKey: SerializationKeys.program7)
    aCoder.encode(seats9ge7, forKey: SerializationKeys.seats9ge7)
    aCoder.encode(program4, forKey: SerializationKeys.program4)
    aCoder.encode(commonAudition1, forKey: SerializationKeys.commonAudition1)
    aCoder.encode(method9, forKey: SerializationKeys.method9)
    aCoder.encode(auditioninformation4, forKey: SerializationKeys.auditioninformation4)
    aCoder.encode(applicants4specialized, forKey: SerializationKeys.applicants4specialized)
    aCoder.encode(requirement15, forKey: SerializationKeys.requirement15)
    aCoder.encode(grade9swdfilledflag10, forKey: SerializationKeys.grade9swdfilledflag10)
    aCoder.encode(admissionspriority62, forKey: SerializationKeys.admissionspriority62)
    aCoder.encode(seats9swd8, forKey: SerializationKeys.seats9swd8)
    aCoder.encode(method3, forKey: SerializationKeys.method3)
    aCoder.encode(grade9swdfilledflag2, forKey: SerializationKeys.grade9swdfilledflag2)
    aCoder.encode(grade9swdapplicants5, forKey: SerializationKeys.grade9swdapplicants5)
    aCoder.encode(offerRate6, forKey: SerializationKeys.offerRate6)
    aCoder.encode(admissionspriority33, forKey: SerializationKeys.admissionspriority33)
    aCoder.encode(primaryAddressLine1, forKey: SerializationKeys.primaryAddressLine1)
    aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
    aCoder.encode(grade9geapplicantsperseat5, forKey: SerializationKeys.grade9geapplicantsperseat5)
    aCoder.encode(seats4specialized, forKey: SerializationKeys.seats4specialized)
    aCoder.encode(overviewParagraph, forKey: SerializationKeys.overviewParagraph)
    aCoder.encode(program6, forKey: SerializationKeys.program6)
    aCoder.encode(grade9gefilledflag5, forKey: SerializationKeys.grade9gefilledflag5)
    aCoder.encode(grades2018, forKey: SerializationKeys.grades2018)
    aCoder.encode(seats6specialized, forKey: SerializationKeys.seats6specialized)
    aCoder.encode(bin, forKey: SerializationKeys.bin)
    aCoder.encode(psalSportsBoys, forKey: SerializationKeys.psalSportsBoys)
    aCoder.encode(grade9gefilledflag4, forKey: SerializationKeys.grade9gefilledflag4)
    aCoder.encode(communityBoard, forKey: SerializationKeys.communityBoard)
    aCoder.encode(grade9gefilledflag7, forKey: SerializationKeys.grade9gefilledflag7)
    aCoder.encode(appperseat1specialized, forKey: SerializationKeys.appperseat1specialized)
    aCoder.encode(requirement51, forKey: SerializationKeys.requirement51)
    aCoder.encode(admissionspriority64, forKey: SerializationKeys.admissionspriority64)
    aCoder.encode(auditioninformation5, forKey: SerializationKeys.auditioninformation5)
    aCoder.encode(grade9geapplicantsperseat8, forKey: SerializationKeys.grade9geapplicantsperseat8)
    aCoder.encode(psalSportsGirls, forKey: SerializationKeys.psalSportsGirls)
    aCoder.encode(grade9geapplicants4, forKey: SerializationKeys.grade9geapplicants4)
    aCoder.encode(grade9swdfilledflag1, forKey: SerializationKeys.grade9swdfilledflag1)
    aCoder.encode(directions6, forKey: SerializationKeys.directions6)
    aCoder.encode(admissionspriority35, forKey: SerializationKeys.admissionspriority35)
    aCoder.encode(dbn, forKey: SerializationKeys.dbn)
    aCoder.encode(grade9geapplicantsperseat7, forKey: SerializationKeys.grade9geapplicantsperseat7)
    aCoder.encode(grade9swdapplicants7, forKey: SerializationKeys.grade9swdapplicants7)
    aCoder.encode(requirement21, forKey: SerializationKeys.requirement21)
    aCoder.encode(code4, forKey: SerializationKeys.code4)
    aCoder.encode(requirement44, forKey: SerializationKeys.requirement44)
    aCoder.encode(seats109, forKey: SerializationKeys.seats109)
    aCoder.encode(program9, forKey: SerializationKeys.program9)
    aCoder.encode(seats104, forKey: SerializationKeys.seats104)
    aCoder.encode(grade9swdfilledflag7, forKey: SerializationKeys.grade9swdfilledflag7)
    aCoder.encode(directions1, forKey: SerializationKeys.directions1)
    aCoder.encode(schoolEmail, forKey: SerializationKeys.schoolEmail)
    aCoder.encode(requirement62, forKey: SerializationKeys.requirement62)
    aCoder.encode(requirement16, forKey: SerializationKeys.requirement16)
    aCoder.encode(requirement23, forKey: SerializationKeys.requirement23)
    aCoder.encode(code2, forKey: SerializationKeys.code2)
    aCoder.encode(international, forKey: SerializationKeys.international)
    aCoder.encode(applicants5specialized, forKey: SerializationKeys.applicants5specialized)
    aCoder.encode(requirement42, forKey: SerializationKeys.requirement42)
    aCoder.encode(appperseat3specialized, forKey: SerializationKeys.appperseat3specialized)
    aCoder.encode(schoolName, forKey: SerializationKeys.schoolName)
    aCoder.encode(collegeCareerRate, forKey: SerializationKeys.collegeCareerRate)
    aCoder.encode(applicants6specialized, forKey: SerializationKeys.applicants6specialized)
    aCoder.encode(seats9swd7, forKey: SerializationKeys.seats9swd7)
    aCoder.encode(grade9geapplicantsperseat1, forKey: SerializationKeys.grade9geapplicantsperseat1)
    aCoder.encode(borough, forKey: SerializationKeys.borough)
    aCoder.encode(admissionspriority54, forKey: SerializationKeys.admissionspriority54)
    aCoder.encode(grade9swdapplicants10, forKey: SerializationKeys.grade9swdapplicants10)
    aCoder.encode(grade9geapplicants1, forKey: SerializationKeys.grade9geapplicants1)
    aCoder.encode(seats9ge6, forKey: SerializationKeys.seats9ge6)
    aCoder.encode(interest4, forKey: SerializationKeys.interest4)
    aCoder.encode(method10, forKey: SerializationKeys.method10)
    aCoder.encode(requirement46, forKey: SerializationKeys.requirement46)
    aCoder.encode(program1, forKey: SerializationKeys.program1)
    aCoder.encode(admissionspriority52, forKey: SerializationKeys.admissionspriority52)
    aCoder.encode(neighborhood, forKey: SerializationKeys.neighborhood)
    aCoder.encode(auditioninformation1, forKey: SerializationKeys.auditioninformation1)
    aCoder.encode(grade9geapplicantsperseat6, forKey: SerializationKeys.grade9geapplicantsperseat6)
    aCoder.encode(prgdesc3, forKey: SerializationKeys.prgdesc3)
    aCoder.encode(seats2specialized, forKey: SerializationKeys.seats2specialized)
    aCoder.encode(requirement57, forKey: SerializationKeys.requirement57)
    aCoder.encode(seats9swd10, forKey: SerializationKeys.seats9swd10)
    aCoder.encode(grade9swdapplicants2, forKey: SerializationKeys.grade9swdapplicants2)
    aCoder.encode(earlycollege, forKey: SerializationKeys.earlycollege)
    aCoder.encode(admissionspriority12, forKey: SerializationKeys.admissionspriority12)
    aCoder.encode(admissionspriority19, forKey: SerializationKeys.admissionspriority19)
    aCoder.encode(code5, forKey: SerializationKeys.code5)
    aCoder.encode(directions2, forKey: SerializationKeys.directions2)
    aCoder.encode(code7, forKey: SerializationKeys.code7)
    aCoder.encode(seats9ge8, forKey: SerializationKeys.seats9ge8)
    aCoder.encode(grade9swdapplicantsperseat9, forKey: SerializationKeys.grade9swdapplicantsperseat9)
    aCoder.encode(schoolSports, forKey: SerializationKeys.schoolSports)
    aCoder.encode(requirement13, forKey: SerializationKeys.requirement13)
    aCoder.encode(grade9geapplicantsperseat9, forKey: SerializationKeys.grade9geapplicantsperseat9)
    aCoder.encode(diplomaendorsements, forKey: SerializationKeys.diplomaendorsements)
    aCoder.encode(interest1, forKey: SerializationKeys.interest1)
    aCoder.encode(auditioninformation6, forKey: SerializationKeys.auditioninformation6)
    aCoder.encode(schoolAccessibilityDescription, forKey: SerializationKeys.schoolAccessibilityDescription)
    aCoder.encode(grade9geapplicants3, forKey: SerializationKeys.grade9geapplicants3)
    aCoder.encode(grade9swdfilledflag9, forKey: SerializationKeys.grade9swdfilledflag9)
    aCoder.encode(program3, forKey: SerializationKeys.program3)
    aCoder.encode(subway, forKey: SerializationKeys.subway)
    aCoder.encode(grade9swdapplicants4, forKey: SerializationKeys.grade9swdapplicants4)
    aCoder.encode(program2, forKey: SerializationKeys.program2)
    aCoder.encode(program10, forKey: SerializationKeys.program10)
    aCoder.encode(offerRate3, forKey: SerializationKeys.offerRate3)
    aCoder.encode(offerRate5, forKey: SerializationKeys.offerRate5)
    aCoder.encode(applicants2specialized, forKey: SerializationKeys.applicants2specialized)
    aCoder.encode(admissionspriority16, forKey: SerializationKeys.admissionspriority16)
    aCoder.encode(grade9geapplicantsperseat10, forKey: SerializationKeys.grade9geapplicantsperseat10)
    aCoder.encode(offerRate8, forKey: SerializationKeys.offerRate8)
    aCoder.encode(sharedSpace, forKey: SerializationKeys.sharedSpace)
    aCoder.encode(requirement55, forKey: SerializationKeys.requirement55)
    aCoder.encode(method6, forKey: SerializationKeys.method6)
    aCoder.encode(admissionspriority18, forKey: SerializationKeys.admissionspriority18)
    aCoder.encode(seats9ge9, forKey: SerializationKeys.seats9ge9)
    aCoder.encode(seats9swd6, forKey: SerializationKeys.seats9swd6)
    aCoder.encode(auditioninformation3, forKey: SerializationKeys.auditioninformation3)
    aCoder.encode(grade9swdapplicants6, forKey: SerializationKeys.grade9swdapplicants6)
    aCoder.encode(admissionspriority26, forKey: SerializationKeys.admissionspriority26)
    aCoder.encode(transfer, forKey: SerializationKeys.transfer)
    aCoder.encode(requirement33, forKey: SerializationKeys.requirement33)
    aCoder.encode(directions3, forKey: SerializationKeys.directions3)
    aCoder.encode(graduationRate, forKey: SerializationKeys.graduationRate)
    aCoder.encode(location, forKey: SerializationKeys.location)
    aCoder.encode(pctStuEnoughVariety, forKey: SerializationKeys.pctStuEnoughVariety)
    aCoder.encode(prgdesc9, forKey: SerializationKeys.prgdesc9)
    aCoder.encode(prgdesc10, forKey: SerializationKeys.prgdesc10)
    aCoder.encode(admissionspriority110, forKey: SerializationKeys.admissionspriority110)
    aCoder.encode(languageClasses, forKey: SerializationKeys.languageClasses)
    aCoder.encode(commonAudition2, forKey: SerializationKeys.commonAudition2)
    aCoder.encode(applicants3specialized, forKey: SerializationKeys.applicants3specialized)
    aCoder.encode(admissionspriority51, forKey: SerializationKeys.admissionspriority51)
    aCoder.encode(seats9ge5, forKey: SerializationKeys.seats9ge5)
    aCoder.encode(requirement37, forKey: SerializationKeys.requirement37)
    aCoder.encode(requirement36, forKey: SerializationKeys.requirement36)
    aCoder.encode(academicopportunities1, forKey: SerializationKeys.academicopportunities1)
    aCoder.encode(pctStuSafe, forKey: SerializationKeys.pctStuSafe)
    aCoder.encode(bus, forKey: SerializationKeys.bus)
    aCoder.encode(requirement12, forKey: SerializationKeys.requirement12)
    aCoder.encode(requirement53, forKey: SerializationKeys.requirement53)
    aCoder.encode(stateCode, forKey: SerializationKeys.stateCode)
    aCoder.encode(grade9geapplicants5, forKey: SerializationKeys.grade9geapplicants5)
    aCoder.encode(seats9swd4, forKey: SerializationKeys.seats9swd4)
    aCoder.encode(code6, forKey: SerializationKeys.code6)
    aCoder.encode(code9, forKey: SerializationKeys.code9)
    aCoder.encode(grade9swdfilledflag4, forKey: SerializationKeys.grade9swdfilledflag4)
    aCoder.encode(grade9swdapplicantsperseat3, forKey: SerializationKeys.grade9swdapplicantsperseat3)
    aCoder.encode(prgdesc5, forKey: SerializationKeys.prgdesc5)
    aCoder.encode(method2, forKey: SerializationKeys.method2)
    aCoder.encode(admissionspriority22, forKey: SerializationKeys.admissionspriority22)
    aCoder.encode(directions7, forKey: SerializationKeys.directions7)
    aCoder.encode(admissionspriority46, forKey: SerializationKeys.admissionspriority46)
    aCoder.encode(offerRate9, forKey: SerializationKeys.offerRate9)
    aCoder.encode(seats107, forKey: SerializationKeys.seats107)
    aCoder.encode(academicopportunities2, forKey: SerializationKeys.academicopportunities2)
    aCoder.encode(grade9swdapplicantsperseat2, forKey: SerializationKeys.grade9swdapplicantsperseat2)
    aCoder.encode(seats102, forKey: SerializationKeys.seats102)
    aCoder.encode(requirement24, forKey: SerializationKeys.requirement24)
    aCoder.encode(method1, forKey: SerializationKeys.method1)
    aCoder.encode(commonAudition4, forKey: SerializationKeys.commonAudition4)
    aCoder.encode(prgdesc8, forKey: SerializationKeys.prgdesc8)
    aCoder.encode(grade9geapplicants10, forKey: SerializationKeys.grade9geapplicants10)
    aCoder.encode(requirement31, forKey: SerializationKeys.requirement31)
    aCoder.encode(grade9swdapplicants9, forKey: SerializationKeys.grade9swdapplicants9)
    aCoder.encode(requirement18, forKey: SerializationKeys.requirement18)
    aCoder.encode(grade9geapplicants9, forKey: SerializationKeys.grade9geapplicants9)
    aCoder.encode(website, forKey: SerializationKeys.website)
    aCoder.encode(interest3, forKey: SerializationKeys.interest3)
    aCoder.encode(commonAudition6, forKey: SerializationKeys.commonAudition6)
    aCoder.encode(requirement54, forKey: SerializationKeys.requirement54)
    aCoder.encode(method8, forKey: SerializationKeys.method8)
    aCoder.encode(admissionspriority36, forKey: SerializationKeys.admissionspriority36)
    aCoder.encode(grade9gefilledflag9, forKey: SerializationKeys.grade9gefilledflag9)
    aCoder.encode(grade9geapplicantsperseat3, forKey: SerializationKeys.grade9geapplicantsperseat3)
    aCoder.encode(grade9gefilledflag10, forKey: SerializationKeys.grade9gefilledflag10)
    aCoder.encode(admissionspriority44, forKey: SerializationKeys.admissionspriority44)
    aCoder.encode(boro, forKey: SerializationKeys.boro)
    aCoder.encode(totalStudents, forKey: SerializationKeys.totalStudents)
    aCoder.encode(interest10, forKey: SerializationKeys.interest10)
    aCoder.encode(grade9geapplicants2, forKey: SerializationKeys.grade9geapplicants2)
    aCoder.encode(ellPrograms, forKey: SerializationKeys.ellPrograms)
    aCoder.encode(auditioninformation2, forKey: SerializationKeys.auditioninformation2)
    aCoder.encode(interest7, forKey: SerializationKeys.interest7)
    aCoder.encode(requirement47, forKey: SerializationKeys.requirement47)
    aCoder.encode(admissionspriority63, forKey: SerializationKeys.admissionspriority63)
    aCoder.encode(grade9geapplicantsperseat4, forKey: SerializationKeys.grade9geapplicantsperseat4)
    aCoder.encode(admissionspriority32, forKey: SerializationKeys.admissionspriority32)
    aCoder.encode(requirement67, forKey: SerializationKeys.requirement67)
    aCoder.encode(seats9ge4, forKey: SerializationKeys.seats9ge4)
    aCoder.encode(finalgrades, forKey: SerializationKeys.finalgrades)
    aCoder.encode(grade9gefilledflag3, forKey: SerializationKeys.grade9gefilledflag3)
    aCoder.encode(appperseat6specialized, forKey: SerializationKeys.appperseat6specialized)
    aCoder.encode(interest6, forKey: SerializationKeys.interest6)
    aCoder.encode(seats9swd1, forKey: SerializationKeys.seats9swd1)
    aCoder.encode(grade9swdapplicantsperseat5, forKey: SerializationKeys.grade9swdapplicantsperseat5)
    aCoder.encode(offerRate1, forKey: SerializationKeys.offerRate1)
    aCoder.encode(extracurricularActivities, forKey: SerializationKeys.extracurricularActivities)
    aCoder.encode(zip, forKey: SerializationKeys.zip)
    aCoder.encode(faxNumber, forKey: SerializationKeys.faxNumber)
    aCoder.encode(endTime, forKey: SerializationKeys.endTime)
    aCoder.encode(requirement35, forKey: SerializationKeys.requirement35)
    aCoder.encode(admissionspriority13, forKey: SerializationKeys.admissionspriority13)
    aCoder.encode(admissionspriority37, forKey: SerializationKeys.admissionspriority37)
    aCoder.encode(auditioninformation7, forKey: SerializationKeys.auditioninformation7)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(prgdesc4, forKey: SerializationKeys.prgdesc4)
    aCoder.encode(eligibility7, forKey: SerializationKeys.eligibility7)
    aCoder.encode(offerRate4, forKey: SerializationKeys.offerRate4)
    aCoder.encode(grade9geapplicants6, forKey: SerializationKeys.grade9geapplicants6)
    aCoder.encode(grade9swdapplicantsperseat8, forKey: SerializationKeys.grade9swdapplicantsperseat8)
    aCoder.encode(seats108, forKey: SerializationKeys.seats108)
    aCoder.encode(admissionspriority74, forKey: SerializationKeys.admissionspriority74)
    aCoder.encode(seats3specialized, forKey: SerializationKeys.seats3specialized)
    aCoder.encode(requirement11, forKey: SerializationKeys.requirement11)
    aCoder.encode(grade9gefilledflag2, forKey: SerializationKeys.grade9gefilledflag2)
    aCoder.encode(seats101, forKey: SerializationKeys.seats101)
    aCoder.encode(interest8, forKey: SerializationKeys.interest8)
    aCoder.encode(commonAudition5, forKey: SerializationKeys.commonAudition5)
    aCoder.encode(appperseat5specialized, forKey: SerializationKeys.appperseat5specialized)
    aCoder.encode(admissionspriority43, forKey: SerializationKeys.admissionspriority43)
    aCoder.encode(interest9, forKey: SerializationKeys.interest9)
    aCoder.encode(eligibility5, forKey: SerializationKeys.eligibility5)
    aCoder.encode(requirement34, forKey: SerializationKeys.requirement34)
    aCoder.encode(grade9swdapplicantsperseat10, forKey: SerializationKeys.grade9swdapplicantsperseat10)
    aCoder.encode(grade9swdfilledflag3, forKey: SerializationKeys.grade9swdfilledflag3)
    aCoder.encode(seats1010, forKey: SerializationKeys.seats1010)
    aCoder.encode(grade9swdfilledflag8, forKey: SerializationKeys.grade9swdfilledflag8)
    aCoder.encode(specialized, forKey: SerializationKeys.specialized)
    aCoder.encode(grade9swdapplicants8, forKey: SerializationKeys.grade9swdapplicants8)
    aCoder.encode(appperseat2specialized, forKey: SerializationKeys.appperseat2specialized)
    aCoder.encode(nta, forKey: SerializationKeys.nta)
    aCoder.encode(academicopportunities3, forKey: SerializationKeys.academicopportunities3)
    aCoder.encode(geoeligibility, forKey: SerializationKeys.geoeligibility)
    aCoder.encode(censusTract, forKey: SerializationKeys.censusTract)
    aCoder.encode(requirement22, forKey: SerializationKeys.requirement22)
    aCoder.encode(pbat, forKey: SerializationKeys.pbat)
    aCoder.encode(requirement38, forKey: SerializationKeys.requirement38)
    aCoder.encode(applicants1specialized, forKey: SerializationKeys.applicants1specialized)
    aCoder.encode(eligibility4, forKey: SerializationKeys.eligibility4)
    aCoder.encode(prgdesc2, forKey: SerializationKeys.prgdesc2)
    aCoder.encode(girls, forKey: SerializationKeys.girls)
    aCoder.encode(method5, forKey: SerializationKeys.method5)
    aCoder.encode(seats9swd5, forKey: SerializationKeys.seats9swd5)
    aCoder.encode(requirement26, forKey: SerializationKeys.requirement26)
    aCoder.encode(academicopportunities5, forKey: SerializationKeys.academicopportunities5)
    aCoder.encode(program8, forKey: SerializationKeys.program8)
    aCoder.encode(admissionspriority71, forKey: SerializationKeys.admissionspriority71)
    aCoder.encode(academicopportunities4, forKey: SerializationKeys.academicopportunities4)
    aCoder.encode(buildingCode, forKey: SerializationKeys.buildingCode)
    aCoder.encode(councilDistrict, forKey: SerializationKeys.councilDistrict)
    aCoder.encode(seats1specialized, forKey: SerializationKeys.seats1specialized)
    aCoder.encode(directions5, forKey: SerializationKeys.directions5)
    aCoder.encode(appperseat4specialized, forKey: SerializationKeys.appperseat4specialized)
    aCoder.encode(admissionspriority28, forKey: SerializationKeys.admissionspriority28)
    aCoder.encode(requirement61, forKey: SerializationKeys.requirement61)
    aCoder.encode(grade9gefilledflag8, forKey: SerializationKeys.grade9gefilledflag8)
    aCoder.encode(admissionspriority23, forKey: SerializationKeys.admissionspriority23)
    aCoder.encode(seats105, forKey: SerializationKeys.seats105)
    aCoder.encode(attendanceRate, forKey: SerializationKeys.attendanceRate)
    aCoder.encode(admissionspriority21, forKey: SerializationKeys.admissionspriority21)
    aCoder.encode(psalSportsCoed, forKey: SerializationKeys.psalSportsCoed)
    aCoder.encode(commonAudition3, forKey: SerializationKeys.commonAudition3)
    aCoder.encode(code10, forKey: SerializationKeys.code10)
    aCoder.encode(offerRate7, forKey: SerializationKeys.offerRate7)
    aCoder.encode(seats9swd9, forKey: SerializationKeys.seats9swd9)
    aCoder.encode(seats9ge2, forKey: SerializationKeys.seats9ge2)
    aCoder.encode(requirement56, forKey: SerializationKeys.requirement56)
    aCoder.encode(admissionspriority14, forKey: SerializationKeys.admissionspriority14)
    aCoder.encode(prgdesc6, forKey: SerializationKeys.prgdesc6)
    aCoder.encode(grade9swdapplicants1, forKey: SerializationKeys.grade9swdapplicants1)
    aCoder.encode(program5, forKey: SerializationKeys.program5)
    aCoder.encode(requirement45, forKey: SerializationKeys.requirement45)
    aCoder.encode(admissionspriority27, forKey: SerializationKeys.admissionspriority27)
    aCoder.encode(seats5specialized, forKey: SerializationKeys.seats5specialized)
    aCoder.encode(advancedplacementCourses, forKey: SerializationKeys.advancedplacementCourses)
    aCoder.encode(admissionspriority42, forKey: SerializationKeys.admissionspriority42)
    aCoder.encode(grade9swdfilledflag5, forKey: SerializationKeys.grade9swdfilledflag5)
    aCoder.encode(admissionspriority34, forKey: SerializationKeys.admissionspriority34)
    aCoder.encode(seats9ge1, forKey: SerializationKeys.seats9ge1)
    aCoder.encode(requirement41, forKey: SerializationKeys.requirement41)
    aCoder.encode(requirement14, forKey: SerializationKeys.requirement14)
    aCoder.encode(grade9swdapplicantsperseat4, forKey: SerializationKeys.grade9swdapplicantsperseat4)
    aCoder.encode(requirement52, forKey: SerializationKeys.requirement52)
    aCoder.encode(admissionspriority31, forKey: SerializationKeys.admissionspriority31)
    aCoder.encode(addtlInfo1, forKey: SerializationKeys.addtlInfo1)
    aCoder.encode(code3, forKey: SerializationKeys.code3)
    aCoder.encode(grade9swdapplicantsperseat7, forKey: SerializationKeys.grade9swdapplicantsperseat7)
    aCoder.encode(requirement63, forKey: SerializationKeys.requirement63)
    aCoder.encode(startTime, forKey: SerializationKeys.startTime)
    aCoder.encode(eligibility6, forKey: SerializationKeys.eligibility6)
    aCoder.encode(seats9ge10, forKey: SerializationKeys.seats9ge10)
    aCoder.encode(requirement25, forKey: SerializationKeys.requirement25)
    aCoder.encode(requirement27, forKey: SerializationKeys.requirement27)
    aCoder.encode(school10thSeats, forKey: SerializationKeys.school10thSeats)
    aCoder.encode(admissionspriority61, forKey: SerializationKeys.admissionspriority61)
    aCoder.encode(seats9swd2, forKey: SerializationKeys.seats9swd2)
    aCoder.encode(ptech, forKey: SerializationKeys.ptech)
    aCoder.encode(prgdesc1, forKey: SerializationKeys.prgdesc1)
    aCoder.encode(admissionspriority29, forKey: SerializationKeys.admissionspriority29)
    aCoder.encode(grade9swdapplicantsperseat1, forKey: SerializationKeys.grade9swdapplicantsperseat1)
    aCoder.encode(seats9ge3, forKey: SerializationKeys.seats9ge3)
    aCoder.encode(admissionspriority11, forKey: SerializationKeys.admissionspriority11)
    aCoder.encode(campusName, forKey: SerializationKeys.campusName)
    aCoder.encode(admissionspriority24, forKey: SerializationKeys.admissionspriority24)
    aCoder.encode(requirement43, forKey: SerializationKeys.requirement43)
    aCoder.encode(boys, forKey: SerializationKeys.boys)
    aCoder.encode(method4, forKey: SerializationKeys.method4)
    aCoder.encode(requirement17, forKey: SerializationKeys.requirement17)
    aCoder.encode(requirement28, forKey: SerializationKeys.requirement28)
  }

}
